﻿using UnityEngine;
using System.Collections.Generic; 
using System.Collections;
using GoogleMobileAds.Api; 
using UnityEngine.SceneManagement;
using System; 

public class AdManager :MonoBehaviour{

   
    float deltaTime; 
      public  bool finalVersion; 


    [Header("AppId")]
    [SerializeField]
    string appId = "ca-app-pub-3352559363227494~2112408705"; 


    [Header("AD Id")] 
    public string bannerId;
    public string internalId;
     public List<string> rewardsId;
    int rewardCount; 

    string bannerIdTest =  "ca-app-pub-3940256099942544/6300978111"; //  GP banner test
    string internalIdTest =  "ca-app-pub-3940256099942544/1033173712";  //GP internal test
    string rewardIdTest = "ca-app-pub-3940256099942544/5224354917"; //Gp reward test

    //All ad mob ADS
    private static BannerView bannerView;
    private InterstitialAd interstitial;
    private static  RewardBasedVideoAd rewardBasedVideo;
    private static string outputMessage = string.Empty;


    private void Start()
    {
        MobileAds.Initialize(appId);
        CreateRequest();
     
        InitAdParam();
    }
   
    void InitAdParam()
    {
           
       RequestInterstitial();
       RequestRewardBasedVideo();
        AdShowManager.Instance.CompleteInitAd(); 
    }


    AdRequest CreateRequest()
    {
       return new AdRequest.Builder().Build();
    }


    /// <summary>
    /// Show banner
    /// </summary>
  
  public  void ShowBanner()
    {
        if (bannerView != null)
        {
            bannerView.Destroy();
        }
        string id = (finalVersion) ? bannerId : bannerIdTest;
        // Create a 320x50 banner at the top of the screen.
        bannerView = new BannerView(id, AdSize.SmartBanner, AdPosition.Bottom);

        bannerView.OnAdLoaded += (senler, argument) => Debug.Log("Banner  Loaded");
        bannerView.OnAdFailedToLoad += (senler, argument) => Debug.Log("Banner  Fail Load");
        bannerView.OnAdOpening += (senler, argument) => Debug.Log("Banner  Opening");
        bannerView.OnAdClosed += (senler, argument) => Debug.Log("Banner Close");
        bannerView.OnAdLeavingApplication += (senler, argument) => Debug.Log("Banner  CloseApply");

        // Load a banner ad.
        bannerView.LoadAd(CreateRequest());
    }
    public void DestroyBanner()
    {
        if (bannerView != null)
        {
            bannerView.Destroy();
        }

    }


    /// <summary>
    /// Reward Request
    /// </summary>
    void RequestRewardBasedVideo()
    {
        if (rewardBasedVideo != null) return;
        rewardBasedVideo = RewardBasedVideoAd.Instance;
        // RewardBasedVideoAd is a singleton, so handlers should only be registered once.
        rewardBasedVideo.OnAdLoaded += (senler, argument) => Debug.Log("Reward video Loaded");
        rewardBasedVideo.OnAdFailedToLoad += (senler, argument) => Debug.Log("Reward video  Fail on Load");
        rewardBasedVideo.OnAdOpening += (senler, argument) => Debug.Log("On  Reward AdOpening");
        rewardBasedVideo.OnAdStarted += (senler, argument) => Debug.Log("Reward video Started");
        rewardBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;
        rewardBasedVideo.OnAdClosed += (senler, argument) => LoadReward() ;
        rewardBasedVideo.OnAdLeavingApplication += (senler, argument) => Debug.Log("Reward video leaving aplication");
        LoadReward();
    }
    public void LoadReward()
    {
        string id;
        if (finalVersion)
        {
            if (rewardsId == null || rewardsId.Count == 0)
            {
                Debug.LogError("Do not Setting reward list in AdManager", this);
                id = rewardIdTest;
                return;
            }
            rewardCount++;
            rewardCount = rewardCount % rewardsId.Count;
            id = rewardsId[rewardCount];

        }
        else
        {
            id = rewardIdTest;
        } 
        rewardBasedVideo.LoadAd(CreateRequest(), id);
        
    }
    void HandleRewardBasedVideoRewarded(object sender, Reward arg)
    {
       // Debug.Log("Reward Ad comlete? get reward");
        LoadReward();
        AdShowManager.Instance.GetRewardNextFrame(); 
    }
   public void ShowRewardBasedVideo()
    {
        if (rewardBasedVideo.IsLoaded()) rewardBasedVideo.Show();
        else
        {
            MonoBehaviour.print("Reward based video ad is not ready yet");
            LoadReward();
        }

    }


    /// <summary>
    /// Internal Request
    /// </summary>
    void RequestInterstitial()
    {
        string id = (finalVersion) ? internalId : internalIdTest; 
        interstitial = new InterstitialAd(id);
       
        interstitial.OnAdLoaded += (senler , argument)=>Debug.Log("INternal Loaded");
        interstitial.OnAdFailedToLoad += (senler, argument) => Debug.Log("INternal Loaded Fale");
        interstitial.OnAdOpening += (senler, argument) => Debug.Log("INternal ad Opened");
        interstitial.OnAdClosed +=  HandleOnAdClosed; 
        this.interstitial.OnAdLeavingApplication += (senler, argument) => Debug.Log("INternal LivingUply");
        this.interstitial.LoadAd(CreateRequest());
    }

    public void HandleOnAdClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdClosed event received");
        AdShowManager.Instance.InternalComplete();
        LoadInternal();
    }

    public void LoadInternal()
    {
        this.interstitial.LoadAd(CreateRequest());
    }
    public bool InternalLoaded()
    {
        return this.interstitial.IsLoaded(); 
    }
     public void ShowInternal()
    {
        if (this.interstitial.IsLoaded())  this.interstitial.Show();
        else  MonoBehaviour.print("Interstitial is not ready yet");    
    }


  
    void OnDestroy()
    {

        if (bannerView != null)
        {
            bannerView.Destroy();
        }
        if (interstitial != null)
        {
            interstitial.Destroy();
        }
    }
    


   #region ADD GUI
    /*
    public void OnGUI()
    {
        GUIStyle style = new GUIStyle();

        Rect rect = new Rect(0, 0, Screen.width, Screen.height);
        style.alignment = TextAnchor.LowerRight;
        style.fontSize = (int)(Screen.height * 0.06);
        style.normal.textColor = new Color(0.0f, 0.0f, 0.5f, 1.0f);
        float fps = 1.0f / this.deltaTime;
        string text = string.Format("{0:0.} fps", fps);
        GUI.Label(rect, text, style);

        // Puts some basic buttons onto the screen.
        GUI.skin.button.fontSize = (int)(0.035f * Screen.width);
        float buttonWidth = 0.35f * Screen.width;
        float buttonHeight = 0.15f * Screen.height;
        float columnOnePosition = 0.1f * Screen.width;
        float columnTwoPosition = 0.55f * Screen.width;

        Rect requestBannerRect = new Rect(
            columnOnePosition,
            0.05f * Screen.height,
            buttonWidth,
            buttonHeight);
        if (GUI.Button(requestBannerRect, "Request\nBanner"))
        {
            this.RequestBanner();
        }

        Rect destroyBannerRect = new Rect(
            columnOnePosition,
            0.225f * Screen.height,
            buttonWidth,
            buttonHeight);
        if (GUI.Button(destroyBannerRect, "Destroy\nBanner"))
        {
            this.bannerView.Destroy();
        }

        Rect requestInterstitialRect = new Rect(
            columnOnePosition,
            0.4f * Screen.height,
            buttonWidth,
            buttonHeight);
        if (GUI.Button(requestInterstitialRect, "Request\nInterstitial"))
        {
            this.RequestInterstitial();
        }

        Rect showInterstitialRect = new Rect(
            columnOnePosition,
            0.575f * Screen.height,
            buttonWidth,
            buttonHeight);
        if (GUI.Button(showInterstitialRect, "Show\nInterstitial"))
        {
            this.ShowInternal();
        }

        Rect destroyInterstitialRect = new Rect(
            columnOnePosition,
            0.75f * Screen.height,
            buttonWidth,
            buttonHeight);
        if (GUI.Button(destroyInterstitialRect, "Destroy\nInterstitial"))
        {
            this.interstitial.Destroy();
        }

        Rect requestRewardedRect = new Rect(
            columnTwoPosition,
            0.05f * Screen.height,
            buttonWidth,
            buttonHeight);
        if (GUI.Button(requestRewardedRect, "Request\nRewarded Video"))
        {
            this.RequestRewardBasedVideo();
        }



        Rect loaddRewardedRect = new Rect(
            columnTwoPosition,
            0.55f * Screen.height,
            buttonWidth,
            buttonHeight);
        if (GUI.Button(loaddRewardedRect, "Load\nRewarded Video"))
        {
            this.LoadReward();
        }



       

        Rect showRewardedRect = new Rect(
            columnTwoPosition,
            0.225f * Screen.height,
            buttonWidth,
            buttonHeight);
        if (GUI.Button(showRewardedRect, "Show\nRewarded Video"))
        {
            this.ShowRewardBasedVideo();
        }

        Rect textOutputRect = new Rect(
            columnTwoPosition,
            0.925f * Screen.height,
            buttonWidth,
            0.05f * Screen.height);
        GUI.Label(textOutputRect, outputMessage);
    }
    
    */
    #endregion
    

  

}

  