﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor; 

[CustomEditor(typeof (AdManager))]
public class AdManagerEditor : Editor {

    public override  void OnInspectorGUI()
    {

       
        EditorGUILayout.BeginHorizontal();
#if FINAL_VERSION
        EditorGUILayout.HelpBox("FINAL_VERSION selected",MessageType.Info);
#else
        EditorGUILayout.HelpBox("If final build you mast USE derective #FINAL_VERSION", MessageType.Warning);
#endif
        EditorGUILayout.EndHorizontal(); 
        EditorGUILayout.Space();
         base.OnInspectorGUI();

    }
}
