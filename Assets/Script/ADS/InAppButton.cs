﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

//[RequireComponent(typeof(Button))]
[RequireComponent(typeof(Image))]
public class InAppButton : MonoBehaviour {


    [Header("IAP Setitng")]
    [SerializeField]
     public bool consumable = true;

    [SerializeField]
    public  PurchaseHandler.IapType type; 

    [SerializeField]
    Text cost;
  
    [SerializeField]
    Button button;

    public void SetCost(string costValue)
    {
        cost.text = costValue.ToString();
    }

    public void Init(PurchaseManager mn)
    {
       
        gameObject.SetActive(true); 
        //button = GetComponent<Button>();
        button.onClick.AddListener(() => mn.BuyInAppIbButton(type.ToString()));
    }
    public void CheckOpen(bool isOpen)
    {
        if (isOpen && !consumable)
        {
            gameObject.SetActive(false);
            return;
        }
        gameObject.SetActive(true); 

    }
    public void IsBuy()
    {
        if (!consumable)
        {
            gameObject.SetActive(false);
        }
    }


     void OnDestroy()
    {
        if (button)
            button.onClick.RemoveAllListeners(); 
    }
}
