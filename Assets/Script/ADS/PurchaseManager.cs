﻿using System;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.UI;
using System.Linq;

public class PurchaseManager : MonoBehaviour, IStoreListener
{
    [Tooltip("ДЛя универсальности кода нужно вбить имя подстроку типа com.snake.tomashevich.")]
    public string productIdPreName;
    private static IStoreController m_StoreController;
    private static IExtensionProvider m_StoreExtensionProvider;
    private int currentProductIndex;
    string currentInAppSelected;


    [Header("Purchase Button")]
    public InAppButton[] allButton;
    /// <summary>
    /// Событие, которое запускается при удачной покупке многоразового товара.
    /// </summary>
    public static event OnSuccessConsumable OnPurchaseConsumable;
    /// <summary>
    /// Событие, которое запускается при удачной покупке не многоразового товара.
    /// </summary>
    public static event OnSuccessNonConsumable OnPurchaseNonConsumable;
    /// <summary>
    /// Событие, которое запускается при неудачной покупке какого-либо товара.
    /// </summary>
    public static event OnFailedPurchase PurchaseFailed;



    // Объявление структуры Receipt для получения
    // информации об IAP.
    [System.Serializable]
    public struct Receipt
    {
        public string Store;
        public string TransactionID;
        public string Payload;
    }

    // Дополнительная информация об IAP для Android.
    [System.Serializable]
    public struct PayloadAndroid
    {
        public string Json;
        public string Signature;
    }



    private void Awake()
    {
        InitializePurchasing();

        //create handler
        PurchaseHandler hanfler = FindObjectOfType<PurchaseHandler>(); 
        hanfler = hanfler?? new GameObject(typeof(PurchaseHandler).ToString(), typeof(PurchaseHandler)).GetComponent<PurchaseHandler>();
        hanfler.transform.parent = transform.parent; 
    }


    /// <summary>
    /// Проверить, куплен ли товар.
    /// </summary>
    /// <param name="id">Индекс товара в списке.</param>
    /// <returns></returns>
    public static bool CheckBuyState(string id)
    {
        Debug.Log("CheckBuyState");
        Product product = m_StoreController.products.WithID(id);
        if (product.hasReceipt) { return true; }
        else { return false; }
    }

    public void InitializePurchasing()
    {
        Debug.Log("InitializePurchasing");
        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
        

        foreach (var iap in allButton)
        {
            bool open = (PlayerPrefs.GetInt(iap.type.ToString(), 0) == 1);
            iap.Init(this);
            builder.AddProduct(iap.type.ToString(), (iap.consumable) ? ProductType.Consumable : ProductType.NonConsumable);
            iap.gameObject.SetActive(false); 
        }
        UnityPurchasing.Initialize(this, builder);
    }


    private bool IsInitialized()
    {
        Debug.Log("IsInitialized = " + (m_StoreController != null && m_StoreExtensionProvider != null));

        return m_StoreController != null && m_StoreExtensionProvider != null;
    }



    public void BuyInAppIbButton(string InAppName)
    {
        currentInAppSelected = InAppName;
        Debug.Log($"Buy InApp by button {InAppName}");
        BuyProductID(InAppName);
    }
    
    void BuyProductID(string productId)
    {
        if (IsInitialized())
        {
            Product product = m_StoreController.products.WithID(productId);

            if (product != null && product.availableToPurchase)
            {
                Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                m_StoreController.InitiatePurchase(product);
                Debug.Log("InitiatePurchase");
            }
            else
            {
                Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
                OnPurchaseFailed(product, PurchaseFailureReason.ProductUnavailable);
            }
        }
    }

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        Debug.Log("OnInitialized: PASS");

        m_StoreController = controller;
        m_StoreExtensionProvider = extensions;



        foreach (var product in m_StoreController.products.all)
        {
            Debug.Log("transactionID = " + product.transactionID +
                       ", definition.id = " + product.definition.id +
                       ", storeSpecificId = " + product.definition.storeSpecificId +
                       ", localizedTitle = " + product.metadata.localizedTitle +
                       ", localizedDescription = " + product.metadata.localizedDescription +
                       ", localizedPriceString = " + product.metadata.localizedPriceString
                       );

            InAppButton button = allButton.FirstOrDefault(p => p.type.ToString() == product.definition.storeSpecificId);
            if (button)
            {
                button.SetCost(product.metadata.localizedPriceString);

                bool open = (PlayerPrefs.GetInt(product.definition.storeSpecificId, 0) == 1);
                button.CheckOpen(open);
            }
              

           
        }

        RestorePurchases();
    }

    public void OnInitializeFailed(InitializationFailureReason error)
    {
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
    }

    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        var product = args.purchasedProduct;
        InAppButton button = allButton.FirstOrDefault(p => p.type.ToString() == args.purchasedProduct.definition.id);
        if (button != null)
        {
            bool consumable = button.consumable;
            if (consumable) OnSuccessC(args);
            else OnSuccessNC(args);

            //  Metrick
            string currency = product.metadata.isoCurrencyCode;
            double price = decimal.ToDouble(product.metadata.localizedPrice);

            /*

            // Создание объекта класса YandexAppMetricaRevenue.
            YandexAppMetricaRevenue revenue = new YandexAppMetricaRevenue(price, currency);
            if (product.receipt != null)
            {
                // Создание объекта класса YandexAppMetricaReceipt.
                YandexAppMetricaReceipt yaReceipt = new YandexAppMetricaReceipt();
                Receipt receipt = JsonUtility.FromJson<Receipt>(product.receipt);
#if UNITY_ANDROID
                PayloadAndroid payloadAndroid = JsonUtility.FromJson<PayloadAndroid>(receipt.Payload);
                yaReceipt.Signature = payloadAndroid.Signature;
                yaReceipt.Data = payloadAndroid.Json;
#elif UNITY_IPHONE
            yaReceipt.TransactionID = receipt.TransactionID;
            yaReceipt.Data = receipt.Payload;
#endif
                revenue.Receipt = yaReceipt;
            }
            // Отправка данных на сервер AppMetrica.
            AppMetrica.Instance.ReportRevenue(revenue);*/
        }

       

            else Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
            return PurchaseProcessingResult.Complete;
    }

    public delegate void OnSuccessConsumable(PurchaseEventArgs args);
    protected virtual void OnSuccessC(PurchaseEventArgs args)
    {
        if (OnPurchaseConsumable != null) OnPurchaseConsumable(args);
        Debug.Log("OnSuccessC " + args.purchasedProduct.definition.id+ " bought!");

    }
    public delegate void OnSuccessNonConsumable(PurchaseEventArgs args);
    protected virtual void OnSuccessNC(PurchaseEventArgs args)
    {
        PlayerPrefs.SetInt(args.purchasedProduct.definition.id, 1);

        InAppButton button = allButton.FirstOrDefault(p => p.type.ToString() == args.purchasedProduct.definition.id);
        if (button != null)
        {
            button.IsBuy();
        }
        
        if (OnPurchaseNonConsumable != null) OnPurchaseNonConsumable(args);
        Debug.Log("OnSuccessNC " + args.purchasedProduct.definition.id + " Buyed!");
    }
    public delegate void OnFailedPurchase(Product product, PurchaseFailureReason failureReason);
    protected virtual void OnFailedP(Product product, PurchaseFailureReason failureReason)
    {
        Debug.Log("OnFailedP");
        if (PurchaseFailed != null) PurchaseFailed(product, failureReason);
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
    }

    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        Debug.Log("OnPurchaseFailed");
        OnFailedP(product, failureReason);
    }

    // Restore purchases previously made by this customer. Some platforms automatically restore purchases, like Google. 
    // Apple currently requires explicit purchase restoration for IAP, conditionally displaying a password prompt.
    public void RestorePurchases()
    {
        // If Purchasing has not yet been set up ...
        if (!IsInitialized())
        {
            // ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
            Debug.Log("RestorePurchases FAIL. Not initialized.");
            return;
        }

        // If we are running on an Apple device ... 
        if (Application.platform == RuntimePlatform.IPhonePlayer ||
            Application.platform == RuntimePlatform.OSXPlayer)
        {
            // ... begin restoring purchases
            Debug.Log("RestorePurchases started ...");

            // Fetch the Apple store-specific subsystem.
            var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
            // Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
            // the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
            apple.RestoreTransactions((result) =>
            {
            // The first phase of restoration. If no more responses are received on ProcessPurchase then 
            // no purchases are available to be restored.
            Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
            });
        }
        // Otherwise ...
        else
        {
            // We are not running on an Apple device. No work is necessary to restore purchases.
            Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
        }
    }
}