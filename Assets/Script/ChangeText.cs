﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CommonEnums; 
public class ChangeText : MonoBehaviour {
    //  нет смысла разбивать это не классы , поведение ондо и тоже 
    
    public ChangeTextType type;

    Text text;

    private void Start()
    {
        text = GetComponentInChildren<Text>();
       

       GameController.Instance.AddTextLisener(type, Change);
    }

    void Change(string value)
    {
        text.text = value; 
    }

}
