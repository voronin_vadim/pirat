﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI; 

public class CoinFly : MonoBehaviour {

    public Color[] colors; 
    Text coinText;
    private void Awake()
    {
        coinText = GetComponentInChildren<Text>(); 

    }

    public  void Init(bigint value , int type)
    {
        gameObject.SetActive(true); 
        if (!coinText) return;
        coinText.text = value.ToString();

        if (colors != null && colors.Length > 0)
        {
            coinText.color = colors[Mathf.Clamp(type, 0, colors.Length - 1)]; 
        }

        transform.DOLocalMoveY(transform.localPosition.y+150f, 0.8f ,true).OnComplete(() => gameObject.SetActive(false)); 
    }
}
