﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using CommonEnums;
using System;



// On this script i'm tire think,   axe work
public class BoosterController : MonoSingleton<BoosterController>
{
    [System.Serializable]
    public class BoosterTimer
    {
        public BoosterType type;

        public int startTimer;
        
        public float timer;
        public bool enable;
        public float enableValue;
       
        public Action onFinish = delegate { };
        
        public List<Text> targetText;

        //save
        const string TIME = "StartBoosterTime";
        const string ENABLE = "BoosterEnable"; 

        public void DecreaseTimer(float delta)

        {
            if (enable)
            {
                if (timer > 0)
                {
                    timer -= delta;
                    TimeSpan span = TimeSpan.FromSeconds(timer);
                    targetText.ForEach(p => p.text = span.Hours.ToString("00") + ":" + span.Minutes.ToString("00") + ":" + span.Seconds.ToString("00"));
                }
                else
                {
                    enable = false;
                    targetText.ForEach(p => p.text = TimeSpan.FromSeconds(startTimer).Hours + "h");
                    onFinish.Invoke();
                    Instance.OnCompleteTimer(type);
                    PlayerPrefs.SetInt(ENABLE + type.ToString(), 0);
                }
            }
        }
        public void CheckReturn()
        {
            bool enableTemp = (PlayerPrefs.GetInt(ENABLE + type.ToString(), 0) == 1);
            targetText.ForEach(p => p.text = TimeSpan.FromSeconds(startTimer).TotalHours + "h");
            if (!enableTemp) return;
            if (!PlayerPrefs.HasKey(TIME + type.ToString())) return;

            //load Timer
            long temp = System.Convert.ToInt64(PlayerPrefs.GetString(TIME + type.ToString()));
            System.DateTime createdTime = System.DateTime.FromBinary(temp);
            System.TimeSpan delta = System.DateTime.Now.Subtract(createdTime);
            int timeLost =   (int)startTimer - (int)delta.TotalSeconds;

            if (timeLost <= 0&& enable)
            {           
                    timer = 0;
                    Debug.Log("Booster Time ending " + type.ToString()); 
            }
            else if (timeLost > 0 && !enable)
            {
                Debug.Log("Restart booster" + type.ToString());
                StartTimer(timeLost);
            }
            else if (timeLost > 0 && enable)
            {
                timer = timeLost; 
            }
        }

        public float GetValue()
        {
            return enable ? enableValue : 1f;
        }

        bool StartTimer(int timerSt)
        {
            if (enable) return false;
            timer =  timerSt; 
            enable = true;   
            return false;
        }
        public void  StartTimerAfterBuy()
        {
            PlayerPrefs.SetString(TIME + type.ToString(), System.DateTime.Now.ToBinary().ToString());
            PlayerPrefs.SetInt(ENABLE + type.ToString(), 1);
            StartTimer(startTimer);
        }

    }
    

   
    public ScrollRect scroll;
    [Tooltip("Slider value then open bublon shop")]
    public float buyDuplonValue = 0.4f;


    public List<iShowObject> boosters;
    public List<iShowObject> dublones;
    public List<iShowObject> golds;
    public List<IParam> dataParam;

    bigint[] goldReward = new bigint[3] { new bigint(50, BigItemEnum.MM), new bigint(5, CommonEnums.BigItemEnum.T), new bigint(1, BigItemEnum.KV) };
    // описание нужно вручную менять


    

    [Header("TIMERS")]

   public List<BoosterTimer> timers;
    float timerTemp; 
    /// <summary>
    /// OPen shop by number
    /// </summary>
    /// <param name="num"> 0 -- golg shop  , 1- dublon shop , 2 booster</param>
    public void OpenShopBooster(int num)
    {
        if(scroll)
        scroll.verticalNormalizedPosition =  Mathf.Min(1f ,  num * buyDuplonValue);
    }

    public void OnCompleteTimer(BoosterType type)
    {
        switch (type)
        {
            case BoosterType.discount:
                TeamController.Instance.UpdateAll(); 
                break; 
        }
    }
    public float GetValue(BoosterType type)
    {
        BoosterTimer tim = timers.FirstOrDefault(p => p.type == type); 
          return  tim?.GetValue() ?? 1f;   // провертиь 
    }

    private void Update()
    {

        // rewrite on Unity RX
        if (timerTemp < 1f) timerTemp += Time.deltaTime;
        else
        {
            timers.ForEach(p => p.DecreaseTimer(timerTemp));
            timerTemp = 0f;
        }
    }
    private void Start()
    {
        Initialize();

       
        timers.ForEach(p => p.CheckReturn());
    }

    // haard code 
    void Initialize()
    {
        List<iShowObject> common = boosters.Concat(dublones).Concat(golds).ToList();
        if (common?.Count != dataParam?.Count || boosters == null)
        {
            Debug.LogError("Not setting  booster and data param", this);
            return;
        }

        int count = common.Count;
        for (int i = 0; i < count; i++)
        {
            iShowObject ob = common[i];
            IParam param = dataParam[i];
            ob.mainImage.sprite = param.imageParam;
            ob.discriptions[0].text = param.GetDiscription()[0];
            ob.discriptions[1].text = param.GetDiscription()[1];
            ob.actionButton[0].onClick.AddListener(() => BoosterClick(ob, param));
            ob.costText[0].text = param.GetCost().cost.ToString();
        }

       

    }

    public void OnApplicationPause(bool pause)
    {
        if (!pause) timers.ForEach(p => p.CheckReturn());
    }

    public void OnApplicationFocus(bool focus)
    {
       
    }

    public void BoosterClick(iShowObject ob , IParam param)
    {
        int index = 0;
        if (boosters.Contains(ob))
        {
            index = boosters.IndexOf(ob);
            if (GameController.Instance.Buy(param.GetCost()))
            {

                Debug.Log("BoosterBuy "  +index.ToString());
                if (timers == null || timers.Count<=index)
                {
                    Debug.Log("Timer do not set", this);
                    return; 
                }
                timers[index].StartTimerAfterBuy(); 

            }

        }
        else if (golds.Contains(ob))
        {
            index = golds.IndexOf(ob);
            if (GameController.Instance.Buy(param.GetCost()))
            {
                Debug.Log("Gold Buy");
                GameController.Instance.AddMoney(goldReward[index], MoneyType.coin); 
            }
        }
        else return; //  IAP processing himself

    }
}
