﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;
using System.Linq;
using System;
using CommonEnums;
//push
using Assets.SimpleAndroidNotifications.Data;
using Assets.SimpleAndroidNotifications.Enums;
using Assets.SimpleAndroidNotifications; 
using Assets.SimpleAndroidNotifications.Helpers;


public class GameController : MonoSingleton<GameController>
{
    public RectTransform piratRtansform;
    System.Action<bool> Click;

    Action<string> onCionChange = delegate { };
    Action<string> onSkullChange = delegate { };
    Action<string> onGrowCahge = delegate { };

    //money Rewrite
    bigint skulls = 0;
    bigint growValue = 0;
    bigint coinsValue = 0;


    public bigint  Coin => coinsValue ; 
    [Header("Coin")]
    public CoinFly coinFly;
    public List<CoinFly> coins; 
    bool inited;
    float currentTimer;

    [Header("Double reward")]
    public Button doubleRewardButton;
    bool doubleActive;
    float timeDounble;
    public Text doubleTimerText;
    public float doubleStartTimer;


    [Header("Chest Param")]
    public float  minutesForChest ;
    bool showChect; 
    
    bigint chestReward;
    public Text chestRewardText;
    public Button collectChest;
    public AdRewardButton doubleChest;

    public  RectTransform canvas;
    //Cheat

    public const string TIMER = "TimerExit"; 
    public const string ENABLE = "TimerEnable";

    public static float cheatReduseCheat = 1f; 
    private void Update()
    {
        if (!inited) return;
        if (Time.time > currentTimer)
        {
            currentTimer = Time.time + 1f;
            AddMoney(growValue, MoneyType.coin);
        }
        if (doubleActive)
        {
            if (timeDounble > 0)
            {
                timeDounble -= Time.deltaTime;
                TimeSpan timer = TimeSpan.FromSeconds(timeDounble);
                doubleTimerText.text = timer.Hours.ToString("00") + " : " + timer.Minutes.ToString("00") + " : " + timer.Seconds.ToString("00");
            }
            else
            {
                doubleActive = false;
                doubleRewardButton.interactable = true;
                doubleTimerText.text = "00:00:00";
                TeamController.Instance.GetGrow();
            }
        }
    }

    public void CheckReturn()
    {
      //  bool enableTemp = (PlayerPrefs.GetInt(TIMER.ToString(), 0) == 1);
      //  if (!enableTemp) return;
        if (!PlayerPrefs.HasKey(TIMER.ToString())) return;

        //load Timer
        long temp = System.Convert.ToInt64(PlayerPrefs.GetString(TIMER.ToString()));
        System.DateTime createdTime = System.DateTime.FromBinary(temp);
        System.TimeSpan delta = System.DateTime.Now.Subtract(createdTime);
        int timeLost = (int)doubleStartTimer - (int)delta.TotalSeconds;

        if (timeLost <= 0 && doubleActive)
        {
            timeDounble = 0;

        }
        else if (timeLost > 0 && !doubleActive)
        {
            ActivateDoubleGrow(timeLost);
        }
        else if (timeLost > 0 && doubleActive)
        {
            timeDounble = timeLost;
        }

    }


    public void RewardTimer2x()
    {
        ActivateDoubleGrow(doubleStartTimer);
        PlayerPrefs.SetString(TIMER, System.DateTime.Now.ToBinary().ToString());
    }

    public void ActivateDoubleGrow(float timer)
    {
        doubleActive = true;
        doubleRewardButton.interactable = false;
        timeDounble += timer;
        TeamController.Instance.GetGrow();
    }
    void SetupPirat()
    {

        EventTrigger eventTrigger = piratRtansform.gameObject.AddComponent(typeof(EventTrigger)) as EventTrigger;
        eventTrigger.AddListener(EventTriggerType.PointerDown, () => OnClick(true));
        eventTrigger.AddListener(EventTriggerType.PointerUp, () => OnClick(false));
    }

    

    public void AddTextLisener(ChangeTextType type, Action<string> val)
    {
        switch (type)
        {
            case ChangeTextType.coin:
                onCionChange += val;
                val(coinsValue.ToString());
                break;

            case ChangeTextType.coinPerSec:
                onGrowCahge += val;
                val(growValue.ToString());
                break;

            case ChangeTextType.skull:
                onSkullChange += val;
                val(skulls.ToString());
                break;
        }
    }



    void Initialize()
    {
        if (inited) return;
        inited = true;

        //currence
        skulls = new bigint(PlayerPrefs.GetString("skullsValue", new bigint(0, BigItemEnum.None).ToSaveString()));
        coinsValue = new bigint(PlayerPrefs.GetString("coinsValue", new bigint(0, BigItemEnum.None).ToSaveString()));
        currentTimer = Time.time + 1f;
        TeamController.Instance.onGrowChange += SetGrow;


        SetupPirat();

        //start show number
        onCionChange(coinsValue.ToString());
        onSkullChange(skulls.ToString());
        TeamController.Instance.GetGrow();

        //Chest
        collectChest.onClick.AddListener(CollectChestReward);
        doubleChest.onShow.AddListener(DoubleChestReward);

        //show ads
        WindowsManager.onNavigationButtonClick += (() => AdShowManager.Instance.Show("buttonClick")); //   Check Button Click


        Facebook.Unity.FB.Init();
        doubleRewardButton.onClick.AddListener(() => {
            TutorialController.Instance.CompleteStep(TutorialController.TutorStepType.TimerClick);
            TutorialController.Instance.BrokeTutorStep(TutorialController.TutorStepType.TimerClick);

        }); 
        coins = new List<CoinFly>(10); 
    }


   

    void SaveCurrency()
    {
        PlayerPrefs.SetString("skullsValue", skulls.ToSaveString());
        PlayerPrefs.SetString("coinsValue", coinsValue.ToSaveString());
    }
    public void AddMoney(bigint value, MoneyType type)
    {
        if (type == MoneyType.coin)
        {
            coinsValue += value;
            onCionChange(coinsValue.ToString());

            if (coinsValue.MoreOrEqual(new bigint(1200)))
                TutorialController.Instance.TryShow(TutorialController.TutorStepType.StarPomClick);

            if (coinsValue.MoreOrEqual(new bigint(1 , BigItemEnum.K)))
                TutorialController.Instance.TryShow(TutorialController.TutorStepType.BuyUpgrade);

        }
        if (type == MoneyType.skull)
        {
            skulls += value;
            onSkullChange(skulls.ToString());
        }
        SaveCurrency();
    }
    public void CheatMultyMoney()
    {
        AddMoney(coinsValue, MoneyType.coin);
    }

    public bool Buy(CostItem cost)
    {
        return Buy(new bigint(cost.cost ,cost.pow), cost.type);
    }
    public bool Buy(bigint value, MoneyType type)
    {
        value.Normalize(); 
        if (type == MoneyType.coin)
        {
            if (coinsValue.MoreOrEqual(value))
            {
                coinsValue -= value;
                onCionChange(coinsValue.ToString());
                return true;
            }
            return false;
        }
        if (type == MoneyType.skull)
        {
            if (skulls.MoreOrEqual(value))
            {
                skulls -= value;
                onSkullChange(skulls.ToString());
                return true;
            }
            return false;
        }
        Debug.LogWarning("Can' check buy Item");
        return false;

    }

    void SetGrow(bigint newgrow)
    {
        float boosterMult = BoosterController.Instance.GetValue(BoosterType.growDouble); 

        growValue = newgrow * (int)boosterMult * ((doubleActive) ? 2 : 1);
        onGrowCahge(growValue.ToString());
    }


    /// <summary>
    /// Click by object
    /// </summary>
    public void OnClick(bool click)
    {
        if (!click) return;
        int mult = 1;
        int i = UnityEngine.Random.Range(0, 10);
        if (i <= 2) mult = 2;
        else if (i <= 4) mult = 3;

        AddMoney(mult * growValue, MoneyType.coin);
        Achiv.Send(AchivType.clickPirat);
        if (!DOTween.IsTweening(piratRtansform))
        piratRtansform.DOShakePosition(0.5f, strength: 5, vibrato: 4, snapping :true,  fadeOut: false);
        SoundManager.PlaySound("clickCoin");

        //coin fly
        CoinFly coin = coins.FirstOrDefault(p => !p.gameObject.activeSelf);
        if (!coin)
        {
            coin = Instantiate(coinFly, piratRtansform);
            coins.Add(coin);
        }

        //coin.transform.position = Camera.main.ScreenToViewportPoint(Input.mousePosition) + Vector3.forward;
      
        float scale = canvas.localScale.x; 
        Vector2 piratStart = canvas.sizeDelta/2f  + piratRtansform.anchoredPosition;
        coin.GetComponent<RectTransform>().anchoredPosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y)/scale -piratStart; 
        coin.Init(mult * growValue , mult-1);

        TutorialController.Instance.CompleteStep(TutorialController.TutorStepType.PiratClick); // unfochinatly   check each time 
        TutorialController.Instance.BrokeTutorStep(TutorialController.TutorStepType.PiratClick); // unfochinatly   check each time 
    }
    private void Awake()
    {
        Initialize();
        OnEnter(); 


    }


    IEnumerator CheckingCheatCoroutine(float timer)
    {
        yield return new  WaitForSeconds(timer);
        CheckChest();
        TeamController.Instance.AddTeamAfterBack(); // add team Only after Chest
    }

    public void DoubleChestReward()  
    {
        doubleChest.gameObject.SetActive(false); 
        chestReward = chestReward*2;
        chestRewardText.text = chestReward.ToString();
    }
    public void CollectChestReward()  
    {
        showChect = false; 
        AddMoney(chestReward, MoneyType.coin);
        Achiv.Send(AchivType.collectMoney); 
       //close current windows set  on button
    }
    void CheckChest()
    {
        if (!PlayerPrefs.HasKey("lastGameOpening")) return;

        //load timer
        long temp = System.Convert.ToInt64(PlayerPrefs.GetString("lastGameOpening"));
        System.DateTime createdTime = System.DateTime.FromBinary(temp);
        System.TimeSpan delta = System.DateTime.Now.Subtract(createdTime);
        int second = (int)delta.TotalSeconds;
        Debug.Log("<color=cyan>" + "Second total = " + second + "</color> ");

        if (showChect) // chect show now
        {
            CollectChestReward();
           
        }

        showChect = second > minutesForChest * 60;

        //windows  manager
        if (showChect)
        {
            doubleRewardButton.gameObject.SetActive(true);

            Achiv.Send(AchivType.returnGame);
            Windows.ShowWindows(NavigationButtonType.ToChest);
            //Windows.ShowWindows(NavigationButtonType.Main  , ()=>Windows.ShowWindows(NavigationButtonType.ToChest));
            
            int chestMaxSize =(int)(UpgradeController.Instance.GetCurrentValue(UpgradeType.chest) * 3600); 
            chestReward = growValue * Mathf.Min ( second , chestMaxSize);
            chestRewardText.text = chestReward.ToString(); 

        }
        else
        {
            if (!growValue.MoreOrEqual(1)) Debug.LogError("No Value for collect chest", this);
            AddMoney(growValue * second, MoneyType.coin);
            Debug.Log("ADD MOney For Chest = " + (growValue * second).ToString()); 

        }

    }

    public void  SendPushMisson(float tempTimer)
    {

        NotificationManager.SendWithAppIcon(TimeSpan.FromSeconds(tempTimer), "Миссия выполнена", "Забрать награду", new Color(0f, 0.6f, 1f), NotificationIcon.Star);

    }
   
    public void OnApplicationPause(bool pause) 
        {
        if (!pause)
        {
            OnEnter(); 
        }
        else
        {
            OnExit(); 
            
        }

        
    }

    void OnEnter()
    {
        CheckReturn();
        StartCoroutine(CheckingCheatCoroutine(1f));
        NotificationManager.CancelAll();
    }
    private void OnExit()
    {

        PlayerPrefs.SetString("lastGameOpening", System.DateTime.Now.ToBinary().ToString());
        PlayerPrefs.Save();

        int chestMaxSize = (int)(UpgradeController.Instance.GetCurrentValue(UpgradeType.chest) * 3600); // chect capacity
        NotificationManager.SendWithAppIcon(TimeSpan.FromSeconds(chestMaxSize * cheatReduseCheat), "Ваш сундук полон", "", new Color(0f, 0.6f, 1f), NotificationIcon.Bell);
    }
    private void OnApplicationQuit()
    {
        OnExit(); 
    }

}
