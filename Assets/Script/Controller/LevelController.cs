﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using CommonEnums;
using UnityEngine.UI;
using DG.Tweening;

public class LevelController : MonoSingleton<LevelController>
{


    [Header("Setup pirat")]
    [Tooltip("Mast order by level Ground")]
    public PiratCustomize[] piratsLevel;

    [Header("Pirats all level In Tree")]  // Must have orderr 1-1 , 2-1,2-2 ,  3_1 ,3_2 
    public Image[] piratLevelsImage;


    [Header("MainImage")]
    public Image piratImage;

    [Header("Level Progress")]
    public Text levelText;
    public Image progressFill;

    [Header("Level Effect")]
    public Image effect;
    public Button levelUp; 

    public List<Tuple<int, int>> levels;  //уровень и нужное колличество армии 
    public List<bigint> levelsValue;
    public int typeCapitan;
    public int currentProgress;

    //  real Level
    [SerializeField]
    int currentLevel;

    //level showing
    [SerializeField]
    int currentLevelShow;

    [SerializeField]
    int levelBuExpirience;



    Action<int> OnLevelChange = delegate { };

    public int CurrentLevel => currentLevelShow;
    public int CurrentLevelInternal => currentLevel;

    private void Initialize()
    {       

        #region LevelInit Tupple

        levels = new List<Tuple<int, int>>(80);
        // за бродяг
        levels.Add(Tuple.Create(0, 2)); // 1
        levels.Add(Tuple.Create(0, 2)); // 2
        levels.Add(Tuple.Create(0, 2)); // 3
        levels.Add(Tuple.Create(0, 2)); // 4
        levels.Add(Tuple.Create(0, 2)); // 5

        //за команды
        levels.Add(Tuple.Create(1, 2)); // 6
        levels.Add(Tuple.Create(1, 2)); // 7
        levels.Add(Tuple.Create(1, 2)); // 8
        levels.Add(Tuple.Create(1, 2)); // 9
        levels.Add(Tuple.Create(1, 2)); // 10

        //за шлюпки
        levels.Add(Tuple.Create(2, 2)); // 11
        levels.Add(Tuple.Create(2, 2)); // 12
        levels.Add(Tuple.Create(2, 2)); // 13
        levels.Add(Tuple.Create(2, 2)); // 14
        levels.Add(Tuple.Create(2, 2)); // 15

        //за шхуны
        levels.Add(Tuple.Create(3, 2)); // 16
        levels.Add(Tuple.Create(3, 2)); // 17
        levels.Add(Tuple.Create(3, 2)); // 18
        levels.Add(Tuple.Create(3, 2)); // 19
        levels.Add(Tuple.Create(3, 2)); // 20


        //за бригантины 
        levels.Add(Tuple.Create(4, 2)); // 21
        levels.Add(Tuple.Create(4, 2)); // 22
        levels.Add(Tuple.Create(4, 2)); // 23
        levels.Add(Tuple.Create(4, 2)); // 24
        levels.Add(Tuple.Create(4, 2)); // 25


        //за каравелы 
        levels.Add(Tuple.Create(5, 2)); // 26
        levels.Add(Tuple.Create(5, 2)); // 27
        levels.Add(Tuple.Create(5, 2)); // 28
        levels.Add(Tuple.Create(5, 2)); // 29
        levels.Add(Tuple.Create(5, 2)); // 30


        //за черные жемчужины 
        levels.Add(Tuple.Create(6, 2)); // 31
        levels.Add(Tuple.Create(6, 1)); // 32
        levels.Add(Tuple.Create(6, 1)); // 33
        levels.Add(Tuple.Create(6, 1)); // 34
        levels.Add(Tuple.Create(6, 1)); // 35
        levels.Add(Tuple.Create(6, 1)); // 36
        levels.Add(Tuple.Create(6, 1)); // 37
        levels.Add(Tuple.Create(6, 1)); // 38
        levels.Add(Tuple.Create(6, 1)); // 39
        levels.Add(Tuple.Create(6, 1)); // 40

        levels.Add(Tuple.Create(6, 2)); // 41   дополнительльная проверка на покупку острова  . После покупки снова нужны жемчужины
        levels.Add(Tuple.Create(6, 2)); // 42
        levels.Add(Tuple.Create(6, 2)); // 43
        levels.Add(Tuple.Create(6, 2)); // 44
        levels.Add(Tuple.Create(6, 2)); // 45
        levels.Add(Tuple.Create(6, 2)); // 46
        levels.Add(Tuple.Create(6, 2)); // 47
        levels.Add(Tuple.Create(6, 2)); // 48
        levels.Add(Tuple.Create(6, 2)); // 49
        levels.Add(Tuple.Create(6, 2)); // 50
        levels.Add(Tuple.Create(6, 2)); // 51
        levels.Add(Tuple.Create(6, 2)); // 52
        levels.Add(Tuple.Create(6, 2)); // 53
        levels.Add(Tuple.Create(6, 2)); // 54
        levels.Add(Tuple.Create(6, 2)); // 55
        levels.Add(Tuple.Create(6, 2)); // 56
        levels.Add(Tuple.Create(6, 2)); // 57
        levels.Add(Tuple.Create(6, 2)); // 58
        levels.Add(Tuple.Create(6, 2)); // 59
        levels.Add(Tuple.Create(6, 2)); // 60

        levels.Add(Tuple.Create(6, 3)); // 61   дополнительльная проверка на покупку острова  . После покупки снова нужны жемчужины
        levels.Add(Tuple.Create(6, 3)); // 62
        levels.Add(Tuple.Create(6, 3)); // 63
        levels.Add(Tuple.Create(6, 3)); // 64
        levels.Add(Tuple.Create(6, 3)); // 65
        levels.Add(Tuple.Create(6, 3)); // 66
        levels.Add(Tuple.Create(6, 3)); // 67
        levels.Add(Tuple.Create(6, 3)); // 68
        levels.Add(Tuple.Create(6, 3)); // 69
        levels.Add(Tuple.Create(6, 3)); // 70
        levels.Add(Tuple.Create(6, 3)); // 71
        levels.Add(Tuple.Create(6, 3)); // 72
        levels.Add(Tuple.Create(6, 3)); // 73
        levels.Add(Tuple.Create(6, 3)); // 74
        levels.Add(Tuple.Create(6, 3)); // 75
        levels.Add(Tuple.Create(6, 3)); // 76
        levels.Add(Tuple.Create(6, 3)); // 77
        levels.Add(Tuple.Create(6, 3)); // 78
        levels.Add(Tuple.Create(6, 3)); // 79
        levels.Add(Tuple.Create(6, 3)); // 80

        #endregion

    }
    // Use this for initialization
    void Start()
    {
       
        if (levelUp)
        {
            levelUp.onClick.AddListener(() => LevelUp()); 
        }
        OnLevelChange(currentLevelShow);
        ShowPirat();
    }
    private void Awake()
    {

        Initialize();
        currentProgress = PlayerPrefs.GetInt("LevelProgress", 0);
        currentLevel = PlayerPrefs.GetInt("Level", 0);
        currentLevelShow = PlayerPrefs.GetInt("LevelShow", 0);
        typeCapitan = PlayerPrefs.GetInt("PiratType", 0);

        if (currentLevelShow>=1)
            TutorialController.Instance.StartBrokeTutorStep(TutorialController.TutorStepType.LevelClick);
        if (typeCapitan>0)
            TutorialController.Instance.StartBrokeTutorStep(TutorialController.TutorStepType.SelecetType);
    }

    public void AddLisenerToCheckLevel(Action<int> action)
    {

        OnLevelChange += action;
        action(currentLevelShow); //  invoke
    }
    void Save()
    {
        PlayerPrefs.SetInt("LevelProgress", currentProgress);
        PlayerPrefs.SetInt("Level", currentLevel);
        PlayerPrefs.SetInt("LevelShow", currentLevelShow);
        PlayerPrefs.SetInt("PiratType", typeCapitan);
    }

    void ShowPirat()
    {
        SetPiratSprite(currentLevelShow, typeCapitan);

        if (levelText) levelText.text = "LVL" + currentLevelShow.ToString();


        float targetFill=0 ; 

        if (currentLevel > currentLevelShow)
        {
            LevelUpAnimationStart(true);
            targetFill = 1f;
        }
        else
        {
            float fakeAddProgress = TeamController.Instance.GetTeamFulling(levels[currentLevel].Item1 - 1);   //fake progress

         // if  (levels[currentLevel].Item2==0)

            targetFill = (currentProgress + fakeAddProgress) / (float)levels[currentLevel].Item2;  
        }
        if (progressFill)
        {
            progressFill.DOKill(); 

            progressFill.DOFillAmount(targetFill, 1f);
        }

    }



    void LevelUp()
    {
        if (currentLevel <= currentLevelShow) return;

        if (currentLevelShow == 40 && typeCapitan < 1)
        {
            TutorialController.Instance.TryShow(TutorialController.TutorStepType.SelecetType);
            Windows.ShowWindows(NavigationButtonType.SelectLevel);
                
            return;
        }

        if (currentLevelShow == 60 && typeCapitan < 3)
        {
            Windows.ShowWindows(NavigationButtonType.SelectLevel);
            return;
        }

        if (currentLevelShow == 80 && typeCapitan < 7)
        {
            Windows.ShowWindows(NavigationButtonType.SelectLevel);
            return;
        }

        currentLevelShow++;
        Achiv.Send(AchivType.levelUp, currentLevelShow);
        LevelUpAnimationStart(false);
        SoundManager.PlaySound("level");
        OnLevelChange(currentLevelShow);
        ShowPirat();
        Save();



        // tutorial 

        TutorialController.Instance.CompleteStep(TutorialController.TutorStepType.LevelClick);

        if(currentLevel == currentLevelShow)
        TutorialController.Instance.Paused(TutorialController.TutorStepType.LevelClick);// если кликнули  не на основном   экране  то паузим  ждем следующий уровень
        

        if (currentLevelShow >= 10)
                TutorialController.Instance.TryShow(TutorialController.TutorStepType.Mission);

        if (currentLevelShow == 40)
            TutorialController.Instance.TryShow(TutorialController.TutorStepType.BuyIsland);





    }

    void LevelUpAnimationStart(bool start)
    {
        if (effect)
        {
            if (start)
            {
                effect.color = Color.white;
                effect.DOKill();
                effect.DOFade(0f, 0.5f).SetLoops(-1, LoopType.Yoyo);
            }
            else
            {
                effect.DOKill();
                effect.DOFade(0f, 0.5f); 
            }
               
          //  effect.DORestart(); 
        }
    }


    // Select pirat Sptire by Type and level
    void SetPiratSprite(int _level, int _typeST)
    {
        //check right input
        if (piratsLevel == null || piratLevelsImage == null || piratsLevel.Length != piratLevelsImage.Length)
        {
            Debug.LogError(" Pirat level  and Pirat image do not accordinate ", this);
            return;
        }

        int count = piratLevelsImage.Length;


        if (_typeST >= count)
        {
            Debug.LogWarning("To mush capitan Type selected " + _typeST, this);
            _typeST = count - 1;
        }

        //set default Image for all level
        for (int i = 0; i < count; i++)
        {
            piratLevelsImage[i].sprite = piratsLevel[i].GetDisableSprite();
        }


        int type = _typeST + 1;  // start with add  1   для того чтобы   можно было разобрать  цепочку выбора уровней .
        int level = _level;

        while ((type >> 1) > 0)  //заход в эту часть возможен только если выбрана какая -нибудь ветка развития 
        {
            type = type >> 1;
            int realType = type - 1; // отнимаем раннее добавленную 1-цу
            piratLevelsImage[realType].sprite = piratsLevel[realType].GetSprite(piratsLevel[realType].LevelCount - 1);  //ставим последний спрайт из уровня .
            level -= piratsLevel[realType].LevelCount;
        }

        // после того как отняли  уровни всех предыдуших типов   смотрим результаты
        Sprite targetSprite;
        if (level < 0)
        {
            Debug.LogWarning("Captain level less then  must bee at  the current level", this);
            targetSprite = piratsLevel[_typeST].GetSprite(0);  //   return  first sprite in current level           
        }
        else if (level >= piratsLevel[_typeST].LevelCount)
        {
            Debug.LogWarning("Captain level bigger then  max on  the current level", this); // type  Capitan do notUpdate
            targetSprite = piratsLevel[_typeST].GetSprite(piratsLevel[_typeST].LevelCount - 1);  //select last Image on level
        }
        else
        {
            targetSprite = piratsLevel[_typeST].GetSprite(level);
        }
        piratImage.sprite = targetSprite;
        piratLevelsImage[_typeST].sprite = targetSprite;

    }



    public void CheckLevel(int armyLevel) // вызывать при покупки армии  для роста уровня.
    {

        int needLevel = levels[currentLevel].Item1;

        if (armyLevel + 1 == needLevel) ShowPirat();   //костыль на  плавный прогресс

        if (armyLevel != needLevel) return;

        // Crutch  on 40 and 60 level
        if (currentLevel == 40 && MapController.Instance.IsLandOpen < 1)          
            return; 
        if (currentLevel == 60 && MapController.Instance.IsLandOpen < 2)
            return;


        currentProgress++;
        if (currentProgress >= levels[currentLevel].Item2)
        {          
            currentProgress = 0;
            currentLevel++;    //  level up       
           TutorialController.Instance.TryShow(TutorialController.TutorStepType.LevelClick);

           
        }
        Save();
        ShowPirat();
    }

    /// <summary>
    /// Gedug Cheat LevelUp
    /// </summary>
    public void CheatLevelUp()
    {
        int needLevel = levels[currentLevel].Item1;
        CheckLevel(needLevel);
    }

    /// <summary>
    /// Select pirat Type  by Button
    /// </summary>
    /// <param name="type">Pirate Type</param>
    public void SelectPiratType(int type)  // function on Button
    {
        int prevType = typeCapitan + 1;  //  прибавляем чтобы отсчет уровня был с 1 
        int nextType = type + 1;   //  прибавляем чтобы отсчет уровня был с 1  

        /*  1
           2 3
         4 5 6 7  */
        //  проверки на уровень нету, потому что она стоит на самих кнопках
        // проверяем ,что можем перейти только на уровень из той же ветки 

        if (nextType >> 1 != prevType)
        {
            Debug.Log("Пытаемся перейти не на свою ветку ");
            return; 
        }

        typeCapitan = type;

        TutorialController.Instance.CompleteStep(TutorialController.TutorStepType.SelecetType);
        TutorialController.Instance.BrokeTutorStep(TutorialController.TutorStepType.SelecetType);  //если   выбрнали уровень не во время тутора то показывать его больше не нужно
        Save();
        SetPiratSprite(currentLevelShow, typeCapitan);
    }
}
