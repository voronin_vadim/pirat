﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CommonEnums;
using System;
using DG.Tweening;
using UnityEngine.SceneManagement;



public class MapController : MonoSingleton<MapController>
{

    bigint[] islandCost;

    int islandOpen;
    [Header("Map and Island")]
    public Image islandImage;
    public Sprite[] islandSprite;

    public Image mainIslandImage;
    public Sprite[] mainIslandSprite; 

    public Text[] islandCostText;

    public int IsLandOpen => islandOpen;



    [Header("Mission")]

    public Text missionTimerText;
    public List<Text> addMissionText; 
    public float missionStartTimer;
    public float missionCurrentTimer;
    float tempTimer;
    [SerializeField]
    bool missionIsActive;
    [SerializeField]
    bool readyForCollect;
    public bigint reward;
    public bigint rewardOnComplete;
    public Text rewardText;

    public List<Image> missionEffects; 


    [Header("Mission Button")]
    [SerializeField]
    Button startMissionButton;
    [SerializeField]
    Button collectRewardButton;
    [SerializeField]
    AdRewardButton rewardButton;


   

    bool inited;

    //static string
    const string TIMELOST = "missionExitTimeLost";
    const string REWARD = "RewardOnComplete";
    const string EXITTIME = "missionExitTime";
    const string ISLAND = "IslandOpen";
    const string READY = "MissionReadyForCollect";


    private void Start()
    {
        Initialize();
        OnEnter();
    }

    void SetMissionActiv(bool active)
    {
        missionIsActive = active;


    }

    void LevelUpAnimationStart(bool start)
    {
        if (start)
            missionEffects.ForEach(p =>
           {
               p.color = Color.white;
               p.DOKill();
               p.DOFade(0f, 0.8f).SetLoops(-1, LoopType.Yoyo);

           });
        else
            missionEffects.ForEach(p =>
            {
                p.DOKill();
                p.DOFade(0f, 0.5f);
            });
        }
    



    private void Update()
    {
        if (missionIsActive)
        {
            tempTimer = missionCurrentTimer - Time.time;
            if (tempTimer > 0)//  rewrite for UNIRX  
                SetTimerText(tempTimer);    
            else
                MissionComplete();
        } 
    }


    public void ButtonShip()
    {
        if (readyForCollect)
        {
            SoundManager.PlaySound("mission");
        }
        TutorialController.Instance.CompleteStep(TutorialController.TutorStepType.Mission);
        TutorialController.Instance.BrokeTutorStep(TutorialController.TutorStepType.Mission); //   если  кликнули  реньше   чем тутор на шаг появился, то нет смысла его показывать
    }

    public void OnOpen()  //  on Button
    {

      
        if (!missionIsActive || !readyForCollect)
        {
            rewardText.text = reward.ToString();
            rewardOnComplete = reward;

            PlayerPrefs.SetString(REWARD, rewardOnComplete.ToSaveString());
        }
        else
        {
            rewardOnComplete = new bigint(PlayerPrefs.GetString(REWARD, "0"));
            rewardText.text = rewardOnComplete.ToString();
        }

    }


    private void SetGrow(bigint grow)
    {
        reward = grow * 1000;

    }
    void MissionComplete()
    {
       
        missionTimerText.text = LocalizationService.Instance.GetTextByKey("complete");
        addMissionText.ForEach(p => p.text =  "" );
        missionIsActive = false;
        readyForCollect = true;
        SetButtonState();

    }

    void SetButtonState()
    {
        LevelUpAnimationStart(readyForCollect); 
        startMissionButton.gameObject.SetActive(!missionIsActive && !readyForCollect);
        collectRewardButton.gameObject.SetActive(missionIsActive || readyForCollect);
        collectRewardButton.interactable = (readyForCollect);
        rewardButton.gameObject.SetActive(!readyForCollect && missionIsActive);

        PlayerPrefs.SetInt(READY, readyForCollect? 1:0);
    }


    void StartMission(float timer)
    {
        //button
        missionIsActive = true;
        SetButtonState();
        missionCurrentTimer = timer + Time.time;
    }

    void GetTimerReward()
    {
        missionCurrentTimer -= 30 * 60;
        Debug.Log("GetReward");
    }


    void CollectReward()
    {
        //reward
        GameController.Instance.AddMoney(rewardOnComplete, MoneyType.coin);
        GameController.Instance.AddMoney(10, MoneyType.skull);
        TeamController.Instance.CheatAddMaxLevelItem();

        readyForCollect = false;
        //button state
        SetButtonState();


        Achiv.Send(AchivType.completeShip); 
        //timer and reward

        OnOpen();
        SetTimerText(missionStartTimer); 
    }

    void SetTimerText(float second )
    {
        TimeSpan timer = TimeSpan.FromSeconds(second);
        missionTimerText.text = timer.Hours.ToString("00") + ":" + timer.Minutes.ToString("00") + ":" + timer.Seconds.ToString("00");     
        addMissionText.ForEach(p => p.text = (second == missionStartTimer)? "": missionTimerText.text); 
    }
    void Initialize()
    {
        if (inited) return;
        islandCost = new bigint[3] { 0, new bigint(10, BigItemEnum.KV), new bigint(1, BigItemEnum.SE) };
        islandOpen = PlayerPrefs.GetInt(ISLAND, 0);
        islandImage.sprite = islandSprite[islandOpen];
        mainIslandImage.sprite = mainIslandSprite[islandOpen]; 

        for (int i = 0; i < islandCostText.Length; i++)
        {
            islandCostText[i].transform.parent.gameObject.SetActive(i > islandOpen);
            islandCostText[i].text = islandCost[i].ToString();
        }

        //Init buttons

        startMissionButton.onClick.AddListener(() => { StartMission(missionStartTimer); SoundManager.PlaySound("missionStart"); });
        collectRewardButton.onClick.AddListener(() => CollectReward());
        rewardButton.onShow.AddListener(GetTimerReward);
        TeamController.Instance.onGrowChange += SetGrow;
        TeamController.Instance.GetGrow();


        readyForCollect = PlayerPrefs.GetInt(READY, 0) == 1; 

        SetButtonState();
        inited = true;

        StartMissionAfterBack();


    }
   
    public void OnApplicationPause(bool pause)
    {
        if (!pause)
        {
            OnEnter(); 
        }
        else
        {
            OnExit(); 
            
        }
    }

    void OnEnter()
    {
        CheckMissionAfterBack();
    }
    private void OnApplicationQuit()
    {
        OnExit();
    }

    private void OnExit()
    {
        PlayerPrefs.SetFloat(TIMELOST, 0f);
        if (missionIsActive)
        {
            float temp = missionCurrentTimer - Time.time;

            PlayerPrefs.SetString(EXITTIME, System.DateTime.Now.ToBinary().ToString());
            PlayerPrefs.SetFloat(TIMELOST, temp);
            PlayerPrefs.Save();

            GameController.Instance.SendPushMisson(temp);


        }
    }
  




    public void CheatMIssionReduse(float time)
    {
        if (missionIsActive)
            missionCurrentTimer = time + Time.time;
    }

    void CheckMissionAfterBack()
    {
        if (missionIsActive)
        {
            missionCurrentTimer -= GetDeltaExit();
        }
        else
        {
            SetTimerText(missionStartTimer);
            
        }
    }
    void StartMissionAfterBack()
    {

        if (!PlayerPrefs.HasKey(EXITTIME)) return;//   return if run first time

        if (readyForCollect) // return if mission complete recently
        {
            MissionComplete();
            return; 
        }

        //load timer
        float temptimerLost = PlayerPrefs.GetFloat(TIMELOST, 0);
        if (temptimerLost <= float.Epsilon) return;
        int second = GetDeltaExit();
       

        //startMission
        StartMission(temptimerLost - second);
    }

    int GetDeltaExit()
    {
        long temp = System.Convert.ToInt64(PlayerPrefs.GetString(EXITTIME));
        System.DateTime createdTime = System.DateTime.FromBinary(temp);
        System.TimeSpan delta = System.DateTime.Now.Subtract(createdTime);
        int second = (int)delta.TotalSeconds;
        Debug.Log("<color=green>" + "Mission time spend = " + second + "</color> ");
        return second;
    }

    void Save()
    {
        PlayerPrefs.SetInt(ISLAND, islandOpen);
    }

    public void OpenIsland(int num)
    {
        if (num <= islandOpen) return;
        // double check ,  also button  must be non interactible  
        if (num == 1 && LevelController.Instance.CurrentLevel >= 40)
        {

        }
        else if (num == 2 && LevelController.Instance.CurrentLevel >= 60)
        {
        }
        else return;
        if (GameController.Instance.Buy(islandCost[num], MoneyType.coin))
        {
            islandOpen = num;
            islandImage.sprite = islandSprite[islandOpen];
            mainIslandImage.sprite = mainIslandSprite[islandOpen];
            islandCostText[islandOpen].transform.parent.gameObject.SetActive(false);
            Save();
            Achiv.Send(AchivType.buyIsland);
            TutorialController.Instance.CompleteStep(TutorialController.TutorStepType.BuyIsland);
            TutorialController.Instance.BrokeTutorStep(TutorialController.TutorStepType.BuyIsland);
        }
    }
}
