﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CommonEnums; 
     



public class SettingManager : MonoBehaviour {

    [Header("Sound Button")]
    public Slider volymeSlider;
    public Toggle soundToogle;
    public Toggle musicToogle;

    [SerializeField]
    Button moreGame; 

    bool enableSound;
    bool enableMusic; 
    float soundValue;


    //PL prefs
    const string STATESOUND = "SoundState";
    const string STATEMUSIC = "MusicState"; 

    const string VALUE = "SoundValue";
    
    private void Start()
    {
        InitParam();
    }
   public  void OnOpen()  // on button
    {
        Achiv.Send(AchivType.spesific, 2);  // 0- buy StarPom , 1- StarPom Work , 2- открыть экран настоек , 5 -открыть окно достижений
    }
    void InitParam()
    {
        //setUpLider
        enableSound = PlayerPrefs.GetInt(STATESOUND, 0) == 0 ;
        enableMusic = PlayerPrefs.GetInt(STATEMUSIC, 0) == 0;

        //SetSound(PlayerPrefs.GetFloat(VALUE, 1f));
      

        soundToogle.isOn = enableSound;
        soundToogle.onValueChanged.AddListener(SoundOn );
        SoundOn(enableSound); 

        musicToogle.isOn = enableMusic;
        musicToogle.onValueChanged.AddListener(MusicOn);
        MusicOn(enableMusic); 


        //slider
        if (volymeSlider)
        {
            volymeSlider.gameObject.SetActive(enableSound);
            volymeSlider.value = soundValue;
            volymeSlider.onValueChanged.AddListener(SoundChadge);
        }

        // more Game
        moreGame.onClick.AddListener(MoreGame); 
    }
    void MoreGame()
    {

        AdShowManager.Instance.Show("moreGame"); 
    }
    void SoundChadge(float value)
    {
        SetSound(value); 
    }

    void MusicOn(bool value)
    {
        enableMusic = value; 
        SoundManager.MusicEnable(value); 
    }
    void SoundOn( bool value)
    {
        enableSound = value; 
        SoundManager.SoundsEnable(value); 
    }
    void SetSound(float value)
    {
        AudioListener.volume = value;
        soundValue = value; 
    }

    private void OnDestroy()
    {

        PlayerPrefs.SetInt(STATESOUND, enableSound? 0: 1);
        PlayerPrefs.SetInt(STATEMUSIC, enableMusic? 0:1);


        PlayerPrefs.SetFloat(VALUE, soundValue);
        PlayerPrefs.Save(); 
    }
}
