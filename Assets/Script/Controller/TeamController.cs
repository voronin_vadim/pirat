﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CommonEnums;
using System.Linq;
using UnityEngine.UI;
using System;

public class TeamController : MonoSingleton<TeamController>
{

    public bool checkLevel = false;

    public Action<bigint> onGrowChange = delegate { };

    [Header("data  Do not CHANGE")]
    [SerializeField]
    int[] team = new int[9];
    public int maxTeam = 10;
    bigint grow;

    // iShowObject prefab  Upgrade team;
    [Header("GererationObject")]
    [Tooltip("Parametr from Scriptable objects")]
    public List<IParam> teamParams; // sctiptableObject of param        
    public IshowTeam teamplateTeam;
    public IshowTeam shopTeam;
    List<IshowTeam> teamOnScene;
    List<IshowTeam> teamOnShop;


    //loadingParam
    float sailorTime = 100; // time for Coming sail 
    float sailorCount = 1; // count to Sail come
    float timeToSpawn; //  tempTimer
    public int maxOpenLevel;

    public Button addTeamButton;  

    [Header("StarCom param")]
    public Text startCopTimer;
    public Button starComButton;
    public int starcomPrise = 1700;
    bool starCop;
    bool starCopTemp;
    float timerStarPom;
    float timerStarPomTemp;

    [Header("Transform with team")]
    public RectTransform teamRectTr;

   

    // String Const
    const string MAX_LEVEL = "maxOpenLevel"; 
    const string TIMER_TEAM = "TeamTimer";
    const string BUY_STAR = "starCopIsBuying"; 

    bool inited = false;


    public void TryBuyStarcom()
    {
        if (GameController.Instance.Buy(starcomPrise, MoneyType.coin))
        {
            starCop = true;
            PlayerPrefs.SetInt(BUY_STAR, 1);
            starComButton.interactable = false;
            EventManager.TriggerAction(NavigationButtonType.CloseWindows); // аналог закрытия по кнопке
            Achiv.Send(AchivType.spesific, 0);  // 0- buy StarPom , 1- StarPom Work , 2- открыть экран настоек , 5 -открыть окно достижений
            CheckStartpomStartWork();
        }
    }


    // Use this for initialization
    void Start()
    {
        if (!inited) Initialize();

    }
    //rewrite
    void Initialize()
    {
        inited = true;
        if (teamParams.Count != team.Length)
        {
            Debug.LogError("Count of team no equal setiin team");
            return;
        }
        teamParams.ForEach(p => p.Init());  // иницилизурем  данные , востанавливаем количество покупок 


        //tutor
        starComButton.onClick.AddListener(() => TutorialController.Instance.CompleteStep(TutorialController.TutorStepType.StarPomClick));
        starComButton.onClick.AddListener(() => TutorialController.Instance.BrokeTutorStep(TutorialController.TutorStepType.StarPomClick));
        addTeamButton.onClick.AddListener ( () => TutorialController.Instance.CompleteStep(TutorialController.TutorStepType.BuyTeam));

        //Load Player Prefs
        for (int i = 0; i < team.Length; i++)
        {
            team[i] = PlayerPrefs.GetInt("TeamCount" + i, 0);
        }
        sailorTime = UpgradeController.Instance.GetCurrentValue(UpgradeType.soldat);
        sailorCount = UpgradeController.Instance.GetCurrentValue(UpgradeType.countS);
        timerStarPom = UpgradeController.Instance.GetCurrentValue(UpgradeType.starcon);
        maxOpenLevel = PlayerPrefs.GetInt(MAX_LEVEL, 0);
        starCop = PlayerPrefs.GetInt(BUY_STAR, 0) == 1;

       

        //Set Start State
        starComButton.interactable = !starCop;
        timeToSpawn = Time.time + sailorTime;
        UpgradeController.OnbuiUpgrade += UpgradeParam;


        //INIT Upgrade Plate

        Transform parrentOb = teamplateTeam.transform.parent;
        teamOnScene = new List<IshowTeam>();
        for (int i = 0; i < team.Length; i++)
        {
            IshowTeam newObj;
            newObj = (i == 0) ? teamplateTeam : Instantiate(teamplateTeam, parrentOb);
            newObj.transform.parent = parrentOb;
            newObj.Init();
            newObj.actionButton[0].onClick.AddListener(() => Upgrade(newObj));   //rewrite
            teamOnScene.Add(newObj);
            teamOnScene[i].mainImage.sprite = teamParams[i].Spr;
        }


        //  Init Buy panel

        parrentOb = shopTeam.transform.parent;
        teamOnShop = new List<IshowTeam>();
        for (int i = 0; i < team.Length; i++)
        {
            IshowTeam newObj;
            newObj = (i == 0) ? shopTeam : Instantiate(shopTeam, parrentOb);
            newObj.transform.parent = parrentOb;
            newObj.Init();
            newObj.actionButton[0].onClick.AddListener(() => Buy(newObj, MoneyType.coin));   //rewrite
            newObj.actionButton[1].onClick.AddListener(() => Buy(newObj, MoneyType.skull));   //rewrite
            teamOnShop.Add(newObj);
            teamOnShop[i].mainImage.sprite = teamParams[i].Spr;

        }
      //  AddTeamAfterBack(); /// ad team 

        UpdateAll();
        GetGrow();
        CheckStartpomStartWork();
    }


    public void UpdateAll()
    {
        for (int i = 0; i < team.Length; i++)
        {
            UpdateViev(i);
            UpdateShopViev(i);
        }

    }
    public void OnOpen()  // on button
    {
        Achiv.Send(AchivType.openTeam); 
    }

    public float GetTeamFulling(int value)
    {
        if (value < 0) return 0;
        if (value >= team.Count())
        {
            Debug.LogWarning("Try  GetTeamFulling with value bigger then team.count", this);
            return 1f;
        }
        return team[value] / (float)maxTeam;
    }

    void UpdateViev(int i)
    {
        if (i < 0) return; 
        IshowTeam soldat = teamOnScene[i];
        soldat.actionButton[0].interactable = (CanUpgrade(i));
        soldat.actionButton[0].gameObject.SetActive(i != 0);  // Button Update close on first item
        TeamParam param = teamParams[i] as TeamParam;
        string[] dis = param.GetDiscription();
        soldat.discriptions[0].text = dis[0];
        soldat.discriptions[1].text = team[i].ToString() + "/" + maxTeam.ToString();

        //count reward
        CostItem rewardCost = param.reward;
        bigint reward = new bigint(rewardCost.cost, rewardCost.pow);
        soldat.discriptions[2].text = "+ " + (reward * team[i]).ToString();

        soldat.fillImage.fillAmount = team[i] / (float)maxTeam;

        if (checkLevel)
            soldat.gameObject.SetActive(i <= maxOpenLevel);
        else
            soldat.gameObject.SetActive(true);
    }

    void UpdateShopViev(int i)
    {
        //string[] value = { mainDiscription, rew, buy };   in TEAM PARAM
        if (i < 0) return;
        IshowTeam soldat = teamOnShop[i];
        IParam param = teamParams[i];

        if (checkLevel)
            soldat.gameObject.SetActive(i < maxOpenLevel - 1);  // viev only team
        else
            soldat.gameObject.SetActive(true);

        if(i==0)
            soldat.gameObject.SetActive(true); // level 0 evaluable 

        string[] dis = param.GetDiscription();
        soldat.discriptions[0].text = dis[0];
        soldat.discriptions[1].text = dis[1];
        soldat.discriptions[2].text = dis[2];
        CostItem cost = param.GetCost(MoneyType.coin);



        soldat.costText[0].text = (new bigint(cost.cost, cost.pow)).ToString();
        soldat.costText[1].text = param.GetCost(MoneyType.skull).cost.ToString(); //не переводим через big int  потому что цена не большая .

    }
    void SaveAll()
    {
        for (int i = 0; i < team.Length; i++)
        {
            PlayerPrefs.SetInt("TeamCount" + i, team[i]);
        }
    }
    void Save(int i)
    {
        PlayerPrefs.SetInt("TeamCount" + i, team[i]);
    }


    void UpgradeParam(IParam param) // Rewrite    что то лишнее
    {
        UpgradeType type = (param as UpgradebleParam).type;
        sailorTime = UpgradeController.Instance.GetCurrentValue(UpgradeType.soldat);
        sailorCount = UpgradeController.Instance.GetCurrentValue(UpgradeType.countS);
        timerStarPom = UpgradeController.Instance.GetCurrentValue(UpgradeType.starcon);
    }


    private void Update()
    {

        if (Time.time > timeToSpawn) // Rewrite for Uni RX
        {
            timeToSpawn = Time.time + sailorTime * BoosterController.Instance.GetValue(BoosterType.delivery);
            Add(0, (int)sailorCount);
        }
        if (starCopTemp)
        {
            if (timerStarPomTemp > 0)
            {
                timerStarPomTemp -= Time.deltaTime;
                TimeSpan span = TimeSpan.FromSeconds(timerStarPomTemp);
                startCopTimer.text = span.Minutes.ToString("00") + ":" + span.Seconds.ToString("00");
            }
            else
            {
                starCopTemp = false;
                timerStarPomTemp = 0f;
                startCopTimer.text = "00:00";
                CheckStartpomStartWork();   // on complete starpom ,check new task
            }
        }
    }

   

    bool CanUpgrade(int i)
    {
        if (i == 0) return false;  // first item upgrade only  automaticly
        if (i == team.Length - 1) return false;  //last army can' t upgrade
        if (team[i] < maxTeam) return false;
        if (IsMax(i+1)) return false;   //check upgrade next level
        return true;
    }
    bool IsMax(int index)
    {
        if (index == team.Length - 1) return false;
        return (team[index] >= maxTeam);

    }
    // BUY Team
    void Buy(iShowObject obj, MoneyType type)
    {
        var t = obj as IshowTeam;
        int index = teamOnShop.IndexOf(t);
        if (team[index] >= maxTeam || (index == team.Length - 1)) return;

        IParam param = teamParams[index];
        CostItem cost = param.GetCost(type);

        if (GameController.Instance.Buy(cost))
        {
            param.BuyItem();
            Add(index, 1);
            UpdateShopViev(index);
            UpdateViev(index - 1);
            CheckStartpomStartWork(); 
        }

    }

    // upgrade team  to Next level
    bool Upgrade(iShowObject obj)
    {
        var t = obj as IshowTeam;
        int index = teamOnScene.IndexOf(t);
        if (IsMax(index + 1)) return false;  //  next Level is full
        Add(index, -team[index]);
        Add(index + 1, 1);
       
        if(index>0)
        SoundManager.PlaySound("buy");

        GetGrow();
        int ind = 0;
        teamOnScene.ForEach(p => { p.actionButton[0].interactable = CanUpgrade(ind); ind++; });

        CheckMaxLevelUp(index + 1);
        CheckStartpomStartWork();
        return true;
    }

    void CheckMaxLevelUp(int index)
    {


        if ((index) > maxOpenLevel) // update max level Open
        {

            Debug.Log("MMMAXX Level" + maxOpenLevel);
            maxOpenLevel = index;
            PlayerPrefs.SetInt(MAX_LEVEL, maxOpenLevel);
            UpdateShopViev(maxOpenLevel - 2);  // Open new team bu buying
            UpdateViev(maxOpenLevel);  // openTeamIn Viev 
            LayoutRebuilder.ForceRebuildLayoutImmediate(teamRectTr);

        

            Achiv.Send(AchivType.combineTeam, index - 1);
            Achiv.Send(AchivType.openArmy, index);
        }
    }
    int GetAnyWork()
    {
        if (!starCop) return 0;
        int upgIndex = 0;
        for (int i = team.Length-1; i > 0; i--)
        {
            if (CanUpgrade(i))
            {
                upgIndex = i;
                break;
            }
        }
        return upgIndex;
    }
    void CheckStartpomStartWork()
    {

        if (starCopTemp) return; // working Now

        int upgIndex = GetAnyWork();
        if (upgIndex == 0) return;

        starCopTemp = true;
        timerStarPomTemp = timerStarPom;
        Upgrade(teamOnScene[upgIndex]);
        Achiv.Send(AchivType.spesific, 1);  // 0- buy StarPom , 1- StarPom Work , 2- открыть экран настоек , 5 -открыть окно достижений
    }


    void Add(int index, int count)
    {


        for (int i = 0; i < count; i++)
        {
            LevelController.Instance.CheckLevel(index);  // CheckFor LevelUp
        }

        int value = team[index] + count;
        if (index == 0 && value >= maxTeam && team[1]<maxTeam)  //automat update first level
        {
            value -= maxTeam;
            Upgrade(teamOnScene[0]);
           
        }

        team[index] = Mathf.Min(value, (index == team.Length - 1) ? int.MaxValue : maxTeam);
        UpdateViev(index);
        Save(index);




        GetGrow();
    }

    public void CheatAddMaxLevelItem()
    {
        Add(maxOpenLevel, 1);
    }


    public void GetGrow()  // reculculate grow
    {
        var reward = teamParams.Cast<TeamParam>().Select(p => new bigint(p.reward.cost, p.reward.pow));
        grow = team.JoinByPosition(reward, (a, b) => a * b).Aggregate(new bigint(0), (current, next) => current + next);
        grow = grow + 1;
        onGrowChange(grow);
    }


    private void OnDestroy()
    {
        UpgradeController.OnbuiUpgrade -= UpgradeParam;
    }


    public void OnApplicationPause(bool pause)

    {
        if (!pause)
        {
            AddTeamAfterBack();
        }

        else
            OnExit();
    }


    private void OnApplicationQuit()
    {
        OnExit(); 
    }

    void OnExit()
    {
        PlayerPrefs.SetString(TIMER_TEAM, System.DateTime.Now.ToBinary().ToString());
        PlayerPrefs.Save();
    }
   public  void AddTeamAfterBack()
    {
        int timer = GetDeltaExit();

        if (starCopTemp && timer < 90)  // if starpomworking
            timerStarPomTemp -= timer;

        if (timer < 10) return;
        if (!inited) Initialize();   //  it maybe  to early 


        int soldat = (int)(timer / sailorTime);
        Debug.Log($" <color = red> Soldat for  time  { soldat} </color>");
        soldat *= (int)sailorCount;
        Debug.Log($" <color = red> All Soldat { soldat} </color>");
        int starpomWorkCount = (int)(timer / timerStarPom);

        Debug.Log($" <color = red> StarPom count { starpomWorkCount} </color>");


        //Starpom work like upper power . And start work with 100 soldat, when
        if (!starCop)
            soldat = Math.Min(soldat, (10 - team[1]) * 10 + (9 - team[0])) ;
        else 
            soldat = Math.Min(soldat, starpomWorkCount * 100);

        int pow = 0;
        int armyOnLevel = 0;
        do
        {
            armyOnLevel = soldat % 10;
            Add(pow, armyOnLevel);

            //  большой   костыль на отслеживаеин роста уровня
            if (pow >= 1)
            {
                for (int i = 0; i < 10; i++)
                {
                    LevelController.Instance.CheckLevel(pow - 1);
                }
            }


            CheckMaxLevelUp(pow);
            pow++;


            // к примеру   добавлили  9 шхун, и было 5,  значит нужно автоматом перевести на след левел
            // starpom wok if can.
            int work = GetAnyWork();
            if (work != 0)
                Upgrade(teamOnScene[work]);
            soldat = soldat / 10;

        }
        while ((soldat) > 0);

    }

    int GetDeltaExit()
    {
        if (!PlayerPrefs.HasKey(TIMER_TEAM.ToString())) return 0;

        long temp = System.Convert.ToInt64(PlayerPrefs.GetString(TIMER_TEAM));
        System.DateTime createdTime = System.DateTime.FromBinary(temp);
        System.TimeSpan delta = System.DateTime.Now.Subtract(createdTime);
        int second = (int)delta.TotalSeconds;
        Debug.Log("<color=green>" + "TEAM time spend = " + second + "</color> ");
        return second;
    }


}
