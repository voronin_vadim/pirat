﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CommonEnums;
using System.Linq;
using System; 

public class UpgradeController : MonoSingleton<UpgradeController>
{


     public static Action<IParam> OnbuiUpgrade;
    public iShowObject prefab ;
    public List<iShowObject> showItems;

    bool inited;     


    public List<IParam> allUpgrade;


        public float GetCurrentValue(UpgradeType type)
        {
            IParam param = allUpgrade.Cast<UpgradebleParam>().FirstOrDefault(p => p.type == type);
            if (param != null) return param.GetValue;
            Debug.LogError("NOt find UpgradebleParam  with name " + type.ToString());
            return 0;
        }

    protected override void Init()
    {
        base.Init();
        if (inited) return; 

        Debug.Log("UpgradeController INIT");
        showItems = new List<iShowObject>();
        Transform parrentOb = prefab.transform.parent;
        allUpgrade.ForEach(p => p.Init()); // задаем стартовые  параметры , считывает данные

        for (int i = 0; i < allUpgrade.Count; i++)
        {
            iShowObject newObj;
            newObj = (i == 0) ? prefab : Instantiate(prefab,parrentOb);
            newObj.transform.parent = parrentOb;
            newObj.SetUpParam(allUpgrade[i]);

            newObj.actionButton[0].onClick.AddListener(() => Buy(newObj, MoneyType.coin));   //rewrite
            newObj.actionButton[1].onClick.AddListener(() => Buy(newObj, MoneyType.skull));   //rewrite

            showItems.Add(newObj); 
        }

    }

    // BUY Upgrade
    void Buy(iShowObject obj, MoneyType type)
    {
        var t = obj as IshowUpgrade;
        int index = showItems.IndexOf(t);
       
        IParam param = allUpgrade[index];
        CostItem cost = param.GetCost(type);

        if (GameController.Instance.Buy(cost))
        {
            param.BuyItem();
            obj.UpdateValue();
            OnbuiUpgrade(param);
            TutorialController.Instance.CompleteStep(TutorialController.TutorStepType.BuyUpgrade); 
            TutorialController.Instance.BrokeTutorStep(TutorialController.TutorStepType.BuyUpgrade);

        }

    }

}
