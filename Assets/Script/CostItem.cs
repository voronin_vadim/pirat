﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CommonEnums;
using System; 

[System.Serializable]
public class CostItem  {

    public int cost;
    public MoneyType type;
    public BigItemEnum pow;

   public  CostItem(int _cost, MoneyType _type, BigItemEnum _pow)
    {
        cost = _cost;
        type = _type;
        pow = _pow; 
    }
    public CostItem(bigint big  , MoneyType _type)
    {
        int max = big.MaxPow(); 
        if (max == 0)
        {     
            cost = big.GetValueIndex(0);
            type = _type;
            pow = BigItemEnum.None;
        }
        else
            {
            cost = big.GetValueIndex(max) * bigint.maxValue + big.GetValueIndex(max - 1);
            type = _type;
            pow = (BigItemEnum)(max - 1);
        }
        
    }

    public static CostItem operator * (CostItem cost, float value)
    {
        float newValue = cost.cost * value;
        BigItemEnum newPov = cost.pow; 
        if (newValue < 1)
        {
            if (cost.pow > 0)
            {
                newPov = cost.pow - 1;
                newValue *= 1000;
            }
        }
        return new CostItem((int)newValue, cost.type, newPov); 
    }

    public static CostItem operator *(float value, CostItem cost)
    {
        return cost * value;
    }
}
