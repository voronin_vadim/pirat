﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using GooglePlayGames;
using GooglePlayGames.BasicApi.SavedGame;
using System;
using GooglePlayGames.BasicApi;


public class GooglePlayServiseController : MonoBehaviour {

    static bool sAutoAuthenticate = true;
    private bool mAuthenticating = false;



    bool nextScene = false; 
    public bool Authenticated
    {
        get
        {
            return Social.Active.localUser.authenticated;
        }
    }
    public void UnlockAchievement(string achId)
    {
        if (Authenticated )
        {
            Debug.Log("  GPS controller  ReportProgress  start"); 
            Social.ReportProgress(achId, 100.0f, (bool success) =>
            {
                Debug.Log("  GPS controller  ReportProgress  result="  + success);
            });
        }
        else
            {

        }
    }
    private void Start()
    {
        Init(); 
    }
    // Use this for initialization
    void Init()
    {  
       // DontDestroyOnLoad(gameObject); 
        // if this is the first time we're running, bring up the sign in flow
        if (sAutoAuthenticate)
        {
            Authenticate();
            sAutoAuthenticate = false;
        }
    }

    private void Update()
    {
        if (nextScene)
        {
            FindObjectOfType<SceneLoader>().LoadScene();
            nextScene = false; 
        }
    }

    public void Authenticate()
    {
        if (Authenticated || mAuthenticating)
        {
            Debug.LogWarning("Ignoring repeated call to Authenticate().");
            nextScene = true;
            return;
        }

        // Enable/disable logs on the PlayGamesPlatform
        PlayGamesPlatform.DebugLogEnabled =true ;

        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
            
            .Build();
        PlayGamesPlatform.InitializeInstance(config);

        // Activate the Play Games platform. This will make it the default
        // implementation of Social.Active
        PlayGamesPlatform.Activate();

       /* // Set the default leaderboard for the leaderboards UI
        ((PlayGamesPlatform)Social.Active).SetDefaultLeaderboardForUI(GameIds.LeaderboardId);*/

        // Sign in to Google Play Games
        mAuthenticating = true;

        if (Social.localUser.authenticated) nextScene = true;
        else
        {
            Social.localUser.Authenticate((bool success) =>
            {
                mAuthenticating = false;
                if (success)
                {
                // if we signed in successfully, load data from cloud
                // Debug.Log("Login successful!");
            }
                else
                {
                // no need to show error message (error messages are shown automatically
                // by plugin)
                // Debug.LogWarning("Failed to sign in with Google Play Games.");
            }
                nextScene = true;
            });
        }
    }


}
