﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class LevelClosedObject : MonoBehaviour {

    [SerializeField]
    int needLevel;

    [SerializeField]
    List<GameObject> startClosedObject;
    [SerializeField]
    List<GameObject> startOpenedObject;
    [SerializeField]
    List<Button> startInteractiveButton;
    [SerializeField]
    List<Button> startDisableButton;


    bool open; 
    private void Awake()
    {
        LevelController.Instance.AddLisenerToCheckLevel(CheckLevel); 
    }

    void CheckLevel(int level)
    {
        open = level >= needLevel;
        startClosedObject.ForEach(p => p.gameObject.SetActive(open)); 
        startOpenedObject.ForEach(p => p.gameObject.SetActive(!open));
        startInteractiveButton.ForEach(p => p.interactable = !open); 
        startDisableButton.ForEach(p => p.interactable = open);

    }
}
