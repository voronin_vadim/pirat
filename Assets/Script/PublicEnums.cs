﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CommonEnums
{


    public enum ChangeTextType
    {
        coin,
        coinPerSec,
        skull
    }



    public enum WindowsTypePopup
    {
        win, popup
    }

    public enum FadeWindowsType
    {

    }

    public enum ButtonObjectType
    {
        Button, Toolle
    }


    public enum ButtonType
    {
        CloseButton,
        ShopButton,
        UpgradeButton,
        ResetButton,
        ExitMainMenuButton,
        RewardButton,
        MapButton
    }

    public enum NavigationButtonType
    {
        Default = 0,
        CloseWindows = 1,
        Menu = 2,
        Shop = 3,
        Pause = 4,
        Map = 5,
        Achieve = 6,
        Upgrade = 7,
        Booster = 8,
        Main = 9,
        InApp = 10,
        NoMoney = 11,
        Team = 12, // Clicker
        Setting = 13,
        SelectItem1 = 14,
        SelectItem2 = 15,
        AddTeam = 16,
        Test = 17,
        SelectLevel = 18,
        ToShip = 22,
        ToChest = 25,
        DoubleReward = 30,
        BuyStarcom = 35,

    }

    public enum WindowsType
    {
        Setting,
        Shop,
        MainBack,
        Map
    }

    public enum ActionEventType
    {
        UpdateMoney,
        UpdateSkull,
        UpdateGrow,
        OpenNoMoney,
        OpenShop,
        UpdateParam,

    }
    public enum BigItemEnum
    {
        None, K, M, MM, T, KV, KI, SE, SI


    }
    public enum UpgradeType
        {
       soldat =0  ,
       chest = 2, 
       starcon = 4 , 
       countS = 6

}
    public enum BoosterType
    {
        growDouble = 0 , 
        discount =1, 
        delivery = 2 , 
    }

    public enum MoneyType
    {
        coin , 
        skull
    }


    public enum AchivType

    {
        clickPirat = 0,
        buyUpgrade =2,
        upgradeToMax = 3, 
        buyArmy= 4,
        collectMoney=6,
        spesific =8, // купить старкома ,Старпом начал работу   , Открыть экран настроек,Открыть окно достижений
        buyStarpom = 10,
        openTeam=12,
        returnGame=14,
        getAchiv=16,
        completeShip =18, 
        buyIsland=20,
        
        combineTeam = 24, 
        levelUp  =26 , 
        openArmy = 28 , 



    }

}
