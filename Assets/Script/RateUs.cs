﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq; 

public class RateUs : MonoBehaviour
{
 

    public GameObject[] starsOn;
    public List<Button> starsOff;
    static bool openApp =false;

    private void Awake()
    { int index = 1;
        starsOff.ForEach(p => { int k = index;  p.onClick.AddListener( ()=>RateAsButton(k));index++; } ); 
    }
    private void OnDisable()
    {
        for (int i = 0; i < 5; i++)
        {
            starsOn[i].gameObject.SetActive(false);
            starsOff[i].gameObject.SetActive(true);
        }
    }

    public void Cancel()
    {
        WindowsManager.CloseCurrentWindowsStatic(); 
    }

    public void Submit()
    {
        Application.OpenURL("https://play.google.com/store/apps/details?id=" + Application.identifier);
        AdShowManager.Instance.ununFocusByManyCauses = true; 
        Cancel(); 
    }

    public void RateAsButton(int n)
    {
        for (int i = 0; i < n; i++)
        {
            starsOn[i].gameObject.SetActive(true);
            starsOff[i].gameObject.SetActive(false);
        }

        for (int i = n; i < 5; i++)
        {
            starsOn[i].gameObject.SetActive(false);
            starsOff[i].gameObject.SetActive(true);
        }

#if UNITY_ANDROID
       
            {
            // Google Review App Normal
           Submit();
        }
     
#endif
    }
}
