﻿
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneLoader : MonoBehaviour
{
    public int sceneInd; 
    public Image Slider;
    public Text progressText; 

    // Use this for initialization
    public void LoadScene()
    {
        StartCoroutine(LoadAsync());
    }
    private void Start()
    {
       // StartCoroutine(LoadAsync()); 
    }

    IEnumerator LoadAsync()
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneInd);
       
        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / .9f);
            Slider.fillAmount = progress;
            if (progressText)
            {
                progressText.text = (progress * 100).ToString("0") + "%"; 
            }
            yield return null;
        }
    }

}


