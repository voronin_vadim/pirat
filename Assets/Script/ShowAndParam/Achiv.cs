﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CommonEnums;
using System;
using DG.Tweening;
using System.Linq; 
public class Achiv : MonoBehaviour
{

    [Header("Scroll")] 
    public ScrollRect scroll; 

    public List<Achivment> achivs;

    public Sprite[] diffScrite;


    public ShowAchiv achivPrefab;
    public List<ShowAchiv> achivOnScene;
    static Dictionary<AchivType, Action<int>> achivAction;

   public  Image effectImage;
    Tween tween;
    bool haveCompleteAchive; 
    bool inited;
   
    private void Awake()
    {
        if (!inited)
            Initilize();
    }
    void StartEffectStar(bool start)
    {
        if (start)
        {
            if (tween == null || !tween.IsPlaying())
                tween = effectImage.transform.DOScale(1.2f, 0.7f).SetLoops(4, LoopType.Yoyo).OnComplete(() => StartEffectStar(true));
        }
        else if (tween != null)
        {
            tween.Kill();
            effectImage.transform.DOScale(1f, 0.5f);
        }

    }



    void Initilize()
    {
        inited = true;
        achivAction = new Dictionary<AchivType, Action<int>>(); 
        Transform parrent = achivPrefab.transform.parent;
        achivOnScene = new List<ShowAchiv>();
        for (int i = 0; i < achivs.Count; i++)
        {
            Achivment achiv = achivs[i];
            achiv.Init();
            ShowAchiv newItem;
            newItem = (i == 0) ? achivPrefab : Instantiate(achivPrefab, parrent);

            string[] disc = achiv.GetDiscription();
            newItem.discriptions[0].text = disc[0];
            newItem.discriptions[1].text = disc[1];
            newItem.costText[0].text = disc[2];
            newItem.mainImage.sprite = diffScrite[achiv.diff];
            
            if (!achiv.isCollected)
            {
                //  fullin gictionary
                if (!achivAction.ContainsKey(achiv.type))
                {
                    Action<int> action = delegate { };
                    action += achiv.Upgrade;  //   ачивка сама подписывается на изменения 
                    achivAction.Add(achiv.type, action);
                }
                else
                {
                    achivAction[achiv.type] += achiv.Upgrade;
                }
                newItem.AddListenerToObj(CollectAchiv);
                achiv.AchivComplete += AchivComplete; // не имеет значение какая ачивка

            }
            achivOnScene.Add(newItem);
            UpgradeViev(i);
        }


        StartEffectStar(achivs.Any(p => p.canCollect));


        CheckForTutor(); 

    }

    void CheckForTutor()
    {
       if (  achivs.Any( p =>p.isCollected)) TutorialController.Instance.StartBrokeTutorStep(TutorialController.TutorStepType.AchivClick);
        foreach (Achivment ach in achivs)
        {
            if (!ach.isCollected&&!ach.canCollect ) continue; 

            switch (ach.name)
            {
                case "AchimentClick1":
                    {
                        TutorialController.Instance.StartBrokeTutorStep(TutorialController.TutorStepType.PiratClick);
                    }
                    break;
                case "AchimentBuyArmy1":
                    {
                        TutorialController.Instance.StartBrokeTutorStep(TutorialController.TutorStepType.BuyTeam);
                    }
                    break;
                case "AchimentBuyIsland1":
                    {
                        TutorialController.Instance.StartBrokeTutorStep(TutorialController.TutorStepType.BuyIsland);
                    }
                    break;
                case "AchimentBuyStarpom":
                    {
                        TutorialController.Instance.StartBrokeTutorStep(TutorialController.TutorStepType.StarPomClick);
                    }
                    break;

                case "AchimentBuyUpgrade1":
                    {
                        TutorialController.Instance.StartBrokeTutorStep(TutorialController.TutorStepType.BuyUpgrade);
                    }
                    break;
                case "AchimentShip1":
                    {
                        TutorialController.Instance.StartBrokeTutorStep(TutorialController.TutorStepType.Mission);
                    }
                    break;
            }
        }
        
        
    }
    void CollectAchiv(iShowObject obj)
    {
        int i = achivOnScene.IndexOf(obj as ShowAchiv);
        Achivment achiv = achivs[i];
       
        if (achiv.canCollect)
        {
            achiv.Collect();
            GameController.Instance.AddMoney(achiv.reward, MoneyType.skull);
            UpgradeViev(i);
            
        }
        StartEffectStar(achivs.Any(p => p.canCollect));

        TutorialController.Instance.CompleteStep(TutorialController.TutorStepType.AchivClick); //  each time 
        TutorialController.Instance.BrokeTutorStep(TutorialController.TutorStepType.AchivClick); //  each time 
    }

    void UpgradeViev(int i)
    {
        Achivment achiv = achivs[i];
        ShowAchiv show = achivOnScene[i];

        float progress = achiv.getProgress;
        bool open = progress >=1f;
        bool collect = achiv.isCollected;
        show.fillImageProgress.fillAmount = progress;
        show.IsOpen(collect);
        show.ReadyForCollect(open && !collect); 
        // rewrite

    }
    void AchivComplete() // не имеет значение какая ачивка
    {
        
        SoundManager.PlaySound("achiv"); 
        haveCompleteAchive = true;
        StartEffectStar(true);
        Achiv.Send(AchivType.getAchiv);
    }

    public  void OnOpen()  // attach on button
    {

        Achiv.Send(AchivType.spesific, 5);  // 0- buy StarPom , 1- StarPom Work , 2- открыть экран настоек , 5 -открыть окно достижений

        if (inited)
        {
            for (int i = 0; i < achivs.Count; i++)
            {
                UpgradeViev(i); 
            }
        }
        Resorted();

       
        
    }

    void Resorted()
    {
        var combine = achivs.Zip(achivOnScene, (data, show) => new  { d =data, s= show });

        combine = combine.OrderByDescending(p => p.d.isCollected).ThenByDescending(t => t.d.canCollect).ThenBy(b=>b.d.name);
        int index = 0;
        combine.ToList().ForEach(p => { p.s.transform.SetSiblingIndex(index); index++; });

        index = combine.FindIndex(p => !p.d.isCollected);
       if(scroll) scroll.verticalNormalizedPosition = 1- index /(float)achivs.Count; 

    }


    public static void Send(AchivType type)
    {
        Send(type, 0); 
}
   public  static void Send(AchivType type, int level)
    {

        if (achivAction == null)
        {
            Debug.Log("Achivment do nit iniitialize");
            return; 
        }
        if (achivAction.ContainsKey(type))
           achivAction[type].Invoke(level); 
    }


}
