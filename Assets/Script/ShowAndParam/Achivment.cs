﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CommonEnums;
using System; 


[CreateAssetMenu(fileName = "Achiment" , menuName = "ObjectGame/Achive", order = 1)]

public class Achivment : ScriptableObject {

    //public string achivName; 
    //public string mainDiscription;
    public AchivType type;
    public int reward; 
    
    public int needLevel;
    public int needCount = 1;


    public Action AchivComplete = delegate { }; 

    public int diff; 
    [SerializeField]
     bool isCollect;

    public int progress;
    string achivPPname;

    const string PROGR = "progres"; 
    const string ISCOLLECT = "collect";

    public string kodeString; 

    public void Upgrade(int level)
    {
        if (level != needLevel) return; 

        progress++;
        if (progress == needCount)  //  gettting achiv
        { AchivComplete.Invoke();

            Debug.Log(" Achivment.cs  Open achiv  " + kodeString.Trim() +"///");

            Social.ReportProgress(kodeString.Trim(), 100.0f, (bool success) =>
            {
                Debug.Log("  GPS controller  ReportProgress  result=" + success);
            });
            
           // FindObjectOfType<GooglePlayServiseController>().UnlockAchievement(kodeString.Trim());
                
                };
        PlayerPrefs.SetInt(achivPPname + PROGR, progress);
        if (progress > needCount) return;
       
    }

    public float getProgress => (progress / (float)needCount);
    public bool isCollected => isCollect;
    public bool canCollect => (!isCollect && progress >= needCount);


    public void Collect()
    {
        if (isCollect)
        {
            Debug.LogError("Twise collect achivment");
            return;  
        }
        PlayerPrefs.SetInt(achivPPname + ISCOLLECT, 1);
        isCollect = true; 
    }
   

    public string[] GetDiscription()
    {

        //loc
        LocalizationService loc = LocalizationService.Instance;

        // string name = loc.GetTextByKey(achivName);
        //string disc  = loc.GetTextByKey(mainDiscription);

        string go_name = name;
        string named = loc.GetTextByKey(go_name + "_Key");
        string disc = loc.GetTextByKey(go_name + "_Val");


        return new string[] { named, disc, reward.ToString() };  
    }

    public void Init()
    {
        achivPPname = type.ToString() + needLevel + "/" + needCount;
      isCollect =   PlayerPrefs.GetInt(achivPPname + ISCOLLECT, 0)==1;
      progress =   PlayerPrefs.GetInt(achivPPname + PROGR, 0);

    }
}
