﻿

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CommonEnums;
using System;
using System.Linq;



[CreateAssetMenu(fileName = "Booster_", menuName = "ObjectGame/booster", order = 1)]
public class BoosterParam : IParam
{

    [SerializeField]
    string mainDiscription;

    [SerializeField]
    string secondDiscription;

    public CostItem costSkull;


    public override void BuyItem()
    {
       
    }

    // interface method 

    public override float GetValue => 0.0f;
    public override void Init()
    {
    }
    public override Sprite Spr => imageParam;
    public override CostItem GetCost(MoneyType? type = null)
    {
            return costSkull;
    }
    public override string[] GetDiscription()
    {
        //loc
        LocalizationService loc = LocalizationService.Instance; 
        string main = loc.GetTextByKey(mainDiscription);
        string sec = loc.HasKey(secondDiscription)?  loc.GetTextByKey(secondDiscription) : secondDiscription;

        string[] value = { main, sec };
        return value;
    }


}




