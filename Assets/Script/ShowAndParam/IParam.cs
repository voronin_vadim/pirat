﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CommonEnums;

abstract public class IParam : ScriptableObject {


    public abstract string[] GetDiscription();
    public abstract CostItem GetCost(MoneyType? type =null);
    public abstract void Init(); 
    public abstract Sprite Spr { get; }
    public virtual float GetValue { get;  }
    public abstract void BuyItem();
    public virtual void InteractItem() { }
    public Sprite imageParam;




}
