﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq; 

public class IshowUpgrade : iShowObject {


    
    public override void SetUpParam(IParam param) // target UpgredeblParam
    {
        myParam = param;
        UpdateText(); 
        

        // if (actionButton != null && (actionButton[0] != null))
        //    actionButton[0].onClick.AddListener(() => Action());
        actionButton.ToList().ForEach(p=>p.onClick.AddListener(() => Action()));

        //else Debug.LogWarning("Do no set action button", this);
        UpdateValue();

    }
     void UpdateText()
    {
        

    }
    public override void UpdateValue()
    {
        base.UpdateValue();

        CostItem costI;
        costI = myParam.GetCost(CommonEnums.MoneyType.coin); 
        costText[0].text = new bigint(costI.cost , costI.pow).ToString();


        costI = myParam.GetCost(CommonEnums.MoneyType.skull);
        costText[1].text = new bigint(costI.cost, costI.pow).ToString();




        string[] discr = myParam.GetDiscription();
        int maxInd = Mathf.Max(discr.Length, discriptions.Length);
        for (int i = 0; i < maxInd; i++)
        {
            discriptions[i].text = discr[i];
        }
        mainImage.sprite = myParam.Spr;

        actionButton[0].gameObject.SetActive(costText[0].text != "0");
        actionButton[1].gameObject.SetActive(costText[1].text != "0");

    }


}
