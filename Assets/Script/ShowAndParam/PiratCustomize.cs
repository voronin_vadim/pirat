﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Pirat", menuName = "ObjectGame/Pirat", order = 1)]
public class PiratCustomize : ScriptableObject {

    public  int LevelCount =>( (piratImages == null) ? 0 : piratImages.Count ) ; 
    public string objectName;
    [SerializeField]
    List<Sprite> piratImages;
    [SerializeField]
    Sprite mainImage;
    [SerializeField]
    Sprite disableImage;
    public Sprite GetSprite(int level)
    {
        if (level >= piratImages.Count) { Debug.LogError($"Do not Find Sprite  with number {level} in {name}  pirat Type"); return mainImage; }
        return piratImages[level];
    }
    public Sprite GetDisableSprite()
    {
        return disableImage; 
    }
    public float GetEffect()
    {
        return 0;
    }
   


}
