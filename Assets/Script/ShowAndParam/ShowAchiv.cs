﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening; 

[System.Serializable]
    public class ShowAchiv : iShowObject
{
    public Image fillImageProgress;
    [SerializeField]
     Image readyForCollect; 

    public override void SetUpParam(IParam param)
    {
        throw new System.NotImplementedException();
    }

    public void Start()
    {
        if (actionButton != null && (actionButton[0] != null))
            actionButton[0].onClick.AddListener(() => Action());
    }

    public void ReadyForCollect( bool ready)
    {
        readyForCollect.gameObject.SetActive(ready); 
      /*  readyForCollect.DOKill();

        if (ready)
        {
            readyForCollect.DOFade(0f, 0f); 
            readyForCollect.DOFade(0.5f, 0.7f).SetLoops(-1, LoopType.Yoyo);     
        }
        else
            readyForCollect.DOFade(0.0f , 0.5f);*/
    }
}
