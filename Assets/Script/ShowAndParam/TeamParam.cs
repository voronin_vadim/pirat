﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CommonEnums;
using System;
using System.Linq;



[CreateAssetMenu(fileName = "TeamLevel_", menuName = "ObjectGame/team", order = 1)]
public class TeamParam : IParam
{
    const string postReward = "income";  //Localization
    const string precount = "buyng";  //Localization
    public float growCoef = 1.1f;

    public string saveName;

    [SerializeField]
    int buying;
    [SerializeField]
    string mainDiscription;

    public CostItem reward;
    public CostItem costCoin;
    public CostItem costSkull;


    public override void BuyItem()
    {
        buying++;
        PlayerPrefs.SetInt("TeamBuying" + saveName, buying);
        PlayerPrefs.Save(); 
        Achiv.Send(AchivType.buyArmy); 

    }

    // interface method 

    public override float GetValue => 0.0f; 
    public override void Init()
    {
        buying = PlayerPrefs.GetInt("TeamBuying" + saveName, 0);
    }
    public override Sprite Spr => imageParam;
    public override CostItem GetCost(MoneyType? type = null)
    {
       
        float boosterMult = BoosterController.Instance.GetValue(BoosterType.discount);

        if (type == MoneyType.skull) return  costSkull * boosterMult;
        if (type == MoneyType.coin)

        {
            bigint value = new bigint( costCoin.cost , costCoin.pow);
            value.Mul(boosterMult); 
            int tempBuying = buying;
            //    crutch  for big Army
            while (tempBuying > 20)
            {
                tempBuying -= 20;
                //value *= (int)Mathf.Pow(growCoef, 20);
                value.Mul(Mathf.Pow(growCoef, 20));
            }
            value.Mul(Mathf.Pow(growCoef, tempBuying));
           
            return new CostItem(value, MoneyType.coin); //  цена за монеты растет
        }
            
        if (type == null)
            return costCoin;
        return costCoin;
    }
    public override string[] GetDiscription()
    {
        var rew = new bigint(reward.cost, reward.pow).ToString();
        rew += LocalizationService.Instance.GetTextByKey(postReward);
        var buy = LocalizationService.Instance.GetTextByKey(precount) + buying;
        var disc = LocalizationService.Instance.GetTextByKey(mainDiscription);

        string[] value = { disc, rew, buy };
        return value;
    }


}



