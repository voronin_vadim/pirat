﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CommonEnums;
using System;
using System.Linq; 



[CreateAssetMenu(fileName = "Upgrade", menuName = "ObjectGame/Upgrade", order = 1)]
public class UpgradebleParam:IParam
{

    int maxlevel = 8;
    int buying;
    [SerializeField]
    float[] allValue;
    [SerializeField]
    string[] valueDiscription;


    // public 
    public UpgradeType type;
    public string paramDiscription;
    public string mainDiscription;
    


    public CostItem[] cost;


    public string GetValueDiscription()
    {
        return (buying >= maxlevel) ? "MAX" : valueDiscription[buying];
    }
    public override void  InteractItem()
    {

    }

    public override void BuyItem()
    {
        buying++;
        buying = Mathf.Min(buying, maxlevel);
        PlayerPrefs.SetInt("UpgradeBuying" + type, buying);

        Achiv.Send(AchivType.buyUpgrade);
        if (buying == maxlevel)
            Achiv.Send(AchivType.upgradeToMax);
    }

    // interface method 

    public override float GetValue => allValue[buying];
    public override void Init()
    {
        buying = PlayerPrefs.GetInt("UpgradeBuying" + type, 0);
    }
    public override Sprite Spr => imageParam;   
    public override CostItem GetCost(MoneyType? type= null)
    {
        type = type??  MoneyType.coin; 
        if (cost[buying].type == type)
            return cost[buying];

        else return new CostItem(0, (MoneyType)(((int)type + 1) % 2), BigItemEnum.None); // Kostil
    }
    public override string[] GetDiscription()
    {
        LocalizationService loc = LocalizationService.Instance;
        string main = loc.GetTextByKey(mainDiscription);
        string disc = loc.GetTextByKey(paramDiscription);

        string[] value = { main, disc, GetValueDiscription() };
        return value; 
    }


}
