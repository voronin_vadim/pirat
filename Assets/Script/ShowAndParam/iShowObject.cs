﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq;


public abstract class iShowObject : MonoBehaviour {


    public Text[] discriptions; 
    public Text[] costText;
    public Button[] actionButton;

    public  List<GameObject> startClosedOb;
    public List<GameObject> startOpenedOb; 
    Action<iShowObject> onAction = delegate { };

    public Image mainImage; 

    public IParam myParam;

    public virtual void UpdateValue()
    {

    }

    public abstract void SetUpParam(IParam param);

    public void  AddListenerToObj( Action<iShowObject> action)
    {
        onAction += action; 

    }
    public  virtual void IsOpen(bool isOpen)
    {
        startClosedOb.ForEach(p => p.SetActive(isOpen)); 
        startOpenedOb.ForEach(p => p.SetActive(!isOpen));
    }
    

    public virtual void Action()
    {
        onAction.Invoke(this); 
    }

}
