﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CommonEnums; 

[RequireComponent(typeof(Text))]
public class ShowText : MonoBehaviour {

    [SerializeField]
    ChangeTextType type; 

    Text text;

    private void Awake()
    {
        text = GetComponent<Text>(); 

    }

    private void Start()
    {
        GameController.Instance.AddTextLisener(type , UpdateText); 
    }

    public void  UpdateText(string value)
    {
        text.text = value; 
    }
}
