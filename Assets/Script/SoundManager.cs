﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
    

public class SoundManager : MonoBehaviour {

    public  AudioClip mainMusic;
    public List<AudioClip> sounds;

  static   AudioSource musicSourse;
  static   List<AudioSource> soundSourse;


    public float musicVolume;
    public float soundVolume;


    public static void SetVolumuMusic(float volume)
    {
        if (musicSourse) musicSourse.volume = volume;
    }
    public static void SetVolumeSounds(float volume)
    {
        if (soundSourse!= null) soundSourse.ForEach(p => p.volume = volume); 
    }

    public static void PlaySound(string soundName)
    {
        AudioSource audio = soundSourse?.FirstOrDefault(p => p.name == soundName);
        if (audio == null)
        {
            Debug.LogWarning("Do not find audio with name " + soundName);
            return; 
        }
        audio.Play(); 
        
    }

    public static void SoundsEnable( bool enable)
    {
        SetVolumeSounds(enable ? 1f : 0f); 
    }
    public static void MusicEnable(bool enable)
    {
        if (!musicSourse) return; 
        if (enable) musicSourse.UnPause();
        else musicSourse.Pause(); 
    }
    private void Awake()
    {
       
    }

    // Use this for initialization
    void Start () {
        CreateSounds();
        GameObject canvas = GameObject.Find("Canvas");
        if (canvas == null) return;
        canvas.GetComponentsInChildren<Button>(true).ToList().ForEach(p => p.onClick.AddListener(() => PlaySound("click"))); 
    }


    void CreateSounds()
    {

        GameObject go;
        soundSourse = new List<AudioSource>();
        if (mainMusic)
        {
            go = new GameObject(mainMusic.name, (typeof(AudioSource)));
            go.transform.SetParent(this.transform);
            musicSourse = go.GetComponent<AudioSource>();
            musicSourse.clip = mainMusic;
            musicSourse.loop = true;
            musicSourse.Play();
        }
        for (int i = 0; i < sounds.Count; i++)
        {
            go = new GameObject(sounds[i].name, (typeof(AudioSource)));
            go.transform.SetParent(this.transform);
            soundSourse.Add(go.GetComponent<AudioSource>());
            soundSourse[i].clip = sounds[i];
            soundSourse[i].playOnAwake = false;


        }
    }
}
