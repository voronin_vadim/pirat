﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 

public class CheatHelp : MonoBehaviour {

    private void Awake()
    {

#if FINAL
        gameObject.SetActive(false); 
#endif
    }


    public void LevelUp()
    {
        LevelController.Instance.CheatLevelUp(); 
    }

    public void Money2X()
    {
        GameController.Instance.CheatMultyMoney(); 

    }

    public void AddMoney(CostItem cost)
    {
        GameController.Instance.AddMoney(new bigint(cost.cost, cost.pow), cost.type); 
    }
    public void AddDublone(int skull)
    {
        GameController.Instance.AddMoney(skull , CommonEnums.MoneyType.skull);
    }
    public void TimeScale(float timeScale)
    {
        Time.timeScale = timeScale; 
    }

    public void AddTeam()
    {
        TeamController.Instance.CheatAddMaxLevelItem(); 

    }

    public void  MissionReduse(float timer)
    {
        MapController.Instance.CheatMIssionReduse(timer); 
    }

    public void CheatMissionReduse(int value)
    {
        GameController.cheatReduseCheat = 1f / (float)value;
    }

    public void  ClearPlayerPrefs()
    {
        PlayerPrefs.DeleteAll();
        SceneManager.LoadScene("main"); 
    }
    public void TestHome()
    {
     WindowsManager.CloseCurrentWindowsStatic();
    }



    public void OpenArmy()
    {
      
        TeamController.Instance.checkLevel = false;
        TeamController.Instance.UpdateAll();
    }
}
