﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Config : MonoBehaviour {

    public string nameC;
    public float floatC;
    public int intC;
    public bool boolC;
    public int[] arrayC;
    public List<int> listC;
    public Hello heelo;

    public Hello[] mass; 

    [System.Serializable]
    public class Hello
    {
        public int drt; 
    }



    public void Start()
    {
        SaveToString(); 
    }
    public string SaveToString()
    {
        string str; 
         str =  JsonUtility.ToJson(this);
        Debug.Log(str);
        return str; 
    }

}
