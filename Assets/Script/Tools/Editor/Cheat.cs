﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;
using CommonEnums; 


public class Cheat  {
    [MenuItem("HelpTools/AddMoney")]
    public static void AddMoney()
    {
        GameController.Instance.AddMoney(500 ,MoneyType.coin); 

    }


    [MenuItem("HelpTools/AddSkull")]
    public static void AddSkull()
    {
        GameController.Instance.AddMoney(50, MoneyType.skull);

    }


    [MenuItem("HelpTools/ClearPlayerPrefs")]
    public static void Clear()
    {
        PlayerPrefs.DeleteAll();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex); 
    }
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
