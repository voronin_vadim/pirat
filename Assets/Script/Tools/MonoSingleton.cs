﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonoSingleton<T> : MonoBehaviour where T:MonoSingleton<T> {

    private static T instance = null;
    public static T Instance
        
    {
        get
        {
            instance = instance ?? (FindObjectOfType(typeof(T)) as T);
            instance = instance ?? new GameObject(typeof(T).ToString(), typeof(T)).GetComponent<T>();
            if (!instance.init) instance.Init();
            return instance;
            
        }
    }
    bool init = false;


    protected virtual void Init()
    {
        init = true;
    }

    private void OnApplicationQuit()
    {
        instance = null; 
    }
}
