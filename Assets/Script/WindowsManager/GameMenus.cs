﻿using System;
using System.Linq;
using UnityEngine;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

/// <summary>
/// GameMenus switcher script v1.4
/// </summary>
[RequireComponent(typeof(CanvasGroup))]
public partial class GameMenus : MonoBehaviour
{
    private static List<GameMenus> _all = null;
    public static List<GameMenus> All
    {
        get
        {
            if (_all == null)
            {
                _all = Resources.FindObjectsOfTypeAll<GameMenus>().ToList();
                SceneManager.sceneLoaded += SceneLoaded;
            }

            return _all;
        }
    }

    public static Tween HideAll(HideAnim? overrideHide = null)
    {
        var allfnd = All.Where(m => m.gameObject.activeInHierarchy).ToList();
        var fnd = allfnd.FirstOrDefault();
        allfnd.Where(m => m != fnd).ToList().ForEach(m => m.Hide(overrideHide ?? m.hideAnim));

        if (fnd)
        {
            var ret = fnd.Hide(overrideHide ?? fnd.hideAnim).SetUpdate(true);
            if (fnd.waitForAnimEnds) return ret;
        }

        return null;
    }

    /// <summary>
    /// Показать это меню
    /// </summary>
    /// <param name="overrideShow">заменить анимацию показа</param>
    /// <param name="overrideHide">заменить анимацию скрытия</param>
    /// <param name="onShowCallback">вызывается когда все анимации закончились</param>
    /// <param name="dontPush">true если не нужно пушить в стэк окон</param>
    /// <returns></returns>
    public GameMenus ShowMe(ShowAnim? overrideShow = null, HideAnim? overrideHide = null, Action onShowCallback = null, bool dontPush = false)
    {
        var curm = CurrentMenu;
        var ps = _lastPopped ?? PeekStack();
        if (curm && !dontPush && curm != ps && this != ps)
            curm.PushStack();

#if UNITY_EDITOR
        var s = "<color=blue>MStack</color>: ";
        if (_menusStack != null)
        {
            foreach (var gameMenu in _menusStack)
                s += "<color=cyan>" + gameMenu.MenuID + "</color>|";

            s = s.Substring(0, s.Length - 1);
        }

        Debug.Log(s);
#endif

        _lastPopped = null;

        var sq = DOTween.Sequence();
        var hd = HideAll(overrideHide);
        if (hd != null)
            sq.Append(hd);

        sq.Append(Show(overrideShow ?? showAnim));

        if (onShowCallback != null) sq.AppendCallback(() => onShowCallback());

        sq.SetUpdate(true);

        _currentMenu = this;

        return this;
    }

    /// <summary>
    /// Показать это меню
    /// </summary>
    /// <param name="overrideShow">заменить анимацию показа</param>
    /// <param name="overrideHide">заменить анимацию скрытия</param>
    /// <param name="onShowCallback">вызывается когда все анимации закончились</param>
    /// <param name="dontPush">true если не нужно пушить в стэк окон</param>
    /// <returns></returns>
    public T ShowMe<T>(ShowAnim? overrideShow = null, HideAnim? overrideHide = null, Action onShowCallback = null, bool dontPush = false)
        where T : GameMenus
    {
        return (T)ShowMe(overrideShow, overrideHide, onShowCallback, dontPush);
    }

    /// <summary>
    /// Показать меню
    /// </summary>
    /// <param name="mid">ID меню</param>
    /// <param name="overrideShow">заменить анимацию показа</param>
    /// <param name="overrideHide">заменить анимацию скрытия</param>
    /// <param name="onShowCallback">вызывается когда все анимации закончились</param>
    /// <param name="dontPush">true если не нужно пушить в стэк окон</param> 
    /// <returns></returns>
    public static GameMenus ShowMenu(string mid, ShowAnim? overrideShow = null, HideAnim? overrideHide = null, Action onShowCallback = null, bool dontPush = false)
    {
        var m = All.Find(x => x.MenuID == mid);
        return !m ? null : m.ShowMe(overrideShow, overrideHide, onShowCallback, dontPush);
    }

    /// <summary>
    /// Показать меню
    /// </summary>
    /// <param name="mid">ID меню</param>
    /// <param name="overrideShow">заменить анимацию показа</param>
    /// <param name="overrideHide">заменить анимацию скрытия</param>
    /// <param name="onShowCallback">вызывается когда все анимации закончились</param>
    /// <param name="dontPush">true если не нужно пушить в стэк окон</param> 
    /// <returns></returns>
    public static T ShowMenu<T>(string mid, ShowAnim? overrideShow = null, HideAnim? overrideHide = null, Action onShowCallback = null, bool dontPush = false)
        where T : GameMenus
    {
        return (T)ShowMenu(mid, overrideShow, overrideHide, onShowCallback);
    }

    /// <summary>
    /// Показать меню
    /// </summary>
    /// <param name="midEnum">ID меню (enum)</param>
    /// <param name="overrideShow">заменить анимацию показа</param>
    /// <param name="overrideHide">заменить анимацию скрытия</param>
    /// <param name="onShowCallback">вызывается когда все анимации закончились</param>
    /// <param name="dontPush">true если не нужно пушить в стэк окон</param> 
    /// <returns></returns>
    public static GameMenus ShowMenu(Enum midEnum, ShowAnim? overrideShow = null, HideAnim? overrideHide = null, Action onShowCallback = null, bool dontPush = false)
    {
        return ShowMenu(midEnum.ToString(), overrideShow, overrideHide, onShowCallback, dontPush);
    }

    /// <summary>
    /// Показать меню
    /// </summary>
    /// <param name="midEnum">ID меню (enum)</param>
    /// <param name="overrideShow">заменить анимацию показа</param>
    /// <param name="overrideHide">заменить анимацию скрытия</param>
    /// <param name="onShowCallback">вызывается когда все анимации закончились</param>
    /// <param name="dontPush">true если не нужно пушить в стэк окон</param> 
    /// <returns></returns>
    public static T ShowMenu<T>(Enum midEnum, ShowAnim? overrideShow = null, HideAnim? overrideHide = null, Action onShowCallback = null, bool dontPush = false)
        where T : GameMenus
    {
        return (T)ShowMenu(midEnum, overrideShow, overrideHide, onShowCallback, dontPush);
    }

    public enum ShowAnim
    {
        None, FadeIn, FadeInPop, FadeInSlideDown
    }
    public enum HideAnim
    {
        None, FadeOut, FadeOutPop, FadeOutSlideUp
    }


    [Delayed]
    public string MenuID;
    public RectTransform body;

    [Header("Animations")]
    public ShowAnim showAnim;
    public HideAnim hideAnim;
    public float showDuration = .4f,
                    hideDuration = .4f;
    public bool waitForAnimEnds = true;

    [Header("Events")]
    public UnityEvent onShow;
    public UnityEvent onHide;

    CanvasGroup _cGroup;
    Vector2 _originalPos;

    bool _initialized = false;

    void Initialize()
    {
        if (_initialized) return;

        _cGroup = GetComponent<CanvasGroup>();
        _originalPos = body.anchoredPosition;

        _initialized = true;

        var _ = All;
    }

    private void Awake()
    {
        Initialize();
    }

    Tween Show(ShowAnim showAnim)
    {
        Initialize();

        _cGroup.alpha = 0;

        var sq = DOTween.Sequence().AppendCallback(() => {
            gameObject.SetActive(true);
        });

        if (showAnim == ShowAnim.None) return sq.AppendCallback(() => _cGroup.alpha = 1);

        sq.AppendCallback(() =>
        {
            body.localScale = Vector3.one;
            body.anchoredPosition = _originalPos;

            _cGroup.blocksRaycasts = false;

            switch (showAnim)
            {
                case ShowAnim.FadeInPop:
                    body.localScale = new Vector3(.7f, .7f, .7f);
                    body.DOScale(Vector3.one, showDuration).SetEase(Ease.OutBack).SetUpdate(true);
                    break;
                case ShowAnim.FadeInSlideDown:
                    body.anchoredPosition = new Vector2(_originalPos.x, _originalPos.y + 150);
                    body.DOAnchorPosY(_originalPos.y, showDuration).SetUpdate(true);
                    break;
            }
        }).Append(_cGroup.DOFade(1, showDuration));

        return sq.AppendCallback(() =>
        {
            _cGroup.blocksRaycasts = true;
            if (onShow != null) onShow.Invoke();
        });
    }

    Tween Hide(HideAnim hideAnim)
    {
        Initialize();

        var sq = DOTween.Sequence();
        if (hideAnim == HideAnim.None)
        {
            sq.AppendCallback(() => gameObject.SetActive(false));
            return sq;
        }

        sq.AppendCallback(() =>
        {
            if (onHide != null) onHide.Invoke();

            _cGroup.blocksRaycasts = false;

            switch (hideAnim)
            {
                case HideAnim.FadeOutPop:
                    body.DOScale(new Vector3(.7f, .7f, .7f), hideDuration).SetEase(Ease.InBack).SetUpdate(true);
                    break;
                case HideAnim.FadeOutSlideUp:
                    body.DOAnchorPosY(_originalPos.y + 150, hideDuration).SetUpdate(true);
                    break;
            }
        }).Append(_cGroup.DOFade(0, hideDuration));

        return sq.AppendCallback(() => gameObject.SetActive(false));
    }

    private static Stack<GameMenus> _menusStack = null;
    private static GameMenus _lastPopped = null;
    public void PushStack()
    {
        if (_menusStack == null)
        {
            _menusStack = new Stack<GameMenus>();
        }
        _menusStack.Push(this);
        _lastPopped = null;
    }

    private static GameMenus _currentMenu;
    public static GameMenus CurrentMenu
    {
        get
        {
            if (!_currentMenu) _currentMenu = FindObjectOfType<GameMenus>();
            return _currentMenu;
        }
    }

    public static GameMenus PopStack()
    {
        _lastPopped = _menusStack != null && _menusStack.Count > 0 ? _menusStack.Pop() : null;
        return _lastPopped;
    }

    public static GameMenus PeekStack()
    {
        return _menusStack != null && _menusStack.Count > 0 ? _menusStack.Peek() : null;
    }

    /// <summary>
    /// ShowMe для событий на кнопках
    /// </summary>
    public void ShowMe()
    {
        ShowMe(null);
    }

    /// <summary>
    /// PopStack + ShowMe + override timeScale для событий на кнопках
    /// </summary>
    /// <param name="setTimescale">-1 - оставить timescale как есть</param>
    public void BackWithTimescale(float setTimescale)
    {
        var m = PopStack();
        if (!m) return;

        m.ShowMe(onShowCallback: setTimescale < 0 ? (Action)null : () => Time.timeScale = setTimescale);
    }

    /// <summary>
    /// PopStack + ShowMe для событий на кнопках
    /// </summary>
    public void Back()
    {
        BackWithTimescale(-1);
    }

    static void SceneLoaded(Scene _, LoadSceneMode __)
    {
        if (_menusStack != null) _menusStack.Clear();
        _menusStack = null;
        _lastPopped = null;
        if (_all != null) _all.Clear();
        _all = null;
        SceneManager.sceneLoaded -= SceneLoaded;
    }
}