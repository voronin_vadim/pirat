﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CommonEnums;
#if UNITYEDITOR
using UnityEditor.AnimatedValues;
using UnityEditor;
#endif
using UnityEngine.Events;
using UnityEngine.EventSystems;

[SelectionBase]
public class SmartButton : MonoBehaviour //, IPointerClickHandler
{


    public ButtonObjectType ObjectType
    {
        get { return _typeObject; }
    }

    public ButtonType buttonType
    {
        get { return _buttonType; }
    }


    [SerializeField]
    private ButtonObjectType _typeObject;

    //  [Header("Button type")]
    [SerializeField]
    private ButtonType _buttonType;

    //   [Header("Target toggle")]
    [SerializeField]
    private Toggle _targetToggle;

    //  [Header("Target Button")]
    [SerializeField]
    private Button _targetButton;

    //[Header("Buttons group: open")]
    [SerializeField]
    private Toggle _targetOpenToggle;

    //[Header("Buttons group: close")]
    [SerializeField]
    private Toggle _targetCloseToggle;

    private string _actionKey = "";
    public string actionKey
    {
        get { return _actionKey; }
        set { _actionKey = value; }
    }

    private string _backActionKey = "";
    public string backActionKey
    {
        get { return _backActionKey; }
        set { _backActionKey = value; }
    }

    private string _defaultActionKey = "";
    public string defaultActionKey
    {
        get { return _defaultActionKey; }
        set { _defaultActionKey = value; }
    }

    private string _defaultBackActionKey = "";
    public string defaultBackActionKey
    {
        get { return _defaultBackActionKey; }
        set { _defaultBackActionKey = value; }
    }

    private string _defaultToggleActionKey = "";
    public string defaultToggleActionKey
    {
        get { return _defaultToggleActionKey; }
        set { _defaultToggleActionKey = value; }
    }

public void Start()
{
        SetupSmartButton(); 
}



    public void SetupSmartButton()
    {
        switch (ObjectType)
        {
            /*case TypeObject.Toggle:
                if (_targetToggle != null)
                {
                    if ((int)buttonType == (int)ButtonsType.OfficeButton || (int)buttonType == (int)ButtonsType.Cam2Toggle)
                    {
                        EventManager.AddActionListening(defaultToggleActionKey, () => { _targetToggle.isOn = true; });
                    }
                    _targetToggle.onValueChanged.AddListener((on) => { OnClick(on); });

                }
                break;*/
            case ButtonObjectType.Button:
                {
                    _targetButton = GetComponent<Button>();
                    if (_targetButton != null)
                    {
                        _targetButton.onClick.AddListener(() => { OnClick(true); });
                    }
                }
               
                break;
           
        }
    }



    void OnClick(bool value)
    {
        if (EventManager.IsContainEvents(buttonType.ToString()))
        {
            EventManager.TriggerAction(buttonType.ToString());
        }

    }
}
/*
public void OnPointerClick(PointerEventData eventData)
 {
    if ((int)ObjectType == (int)TypeObject.Toggle)
    {
        if (_targetToggle.isOn == false)
        {
            if (EventManager.isContainsEvents(defaultActionKey))
            {
                Debug.Log("DefaultAction " + defaultActionKey);
                EventManager.TriggerActionEvent(defaultActionKey);

                if (EventManager.isContainsEvents(defaultToggleActionKey))
                {
                    Debug.Log("DefaultToggleAction " + defaultToggleActionKey);
                    EventManager.TriggerActionEvent(defaultToggleActionKey);
                }
            }
        } 
    }
    else if ((int)ObjectType == (int)TypeObject.DoorTogglesGroup)
    {
        if (_targetOpenToggle.isOn == false || _targetCloseToggle.isOn == false)
        {
            if (EventManager.isContainsEvents(defaultActionKey))
            {
                Debug.Log("DefaultAction " + defaultActionKey);
                EventManager.TriggerActionEvent(defaultActionKey);

                if (EventManager.isContainsEvents(defaultToggleActionKey))
                {
                    Debug.Log("DefaultToggleAction " + defaultToggleActionKey);
                    EventManager.TriggerActionEvent(defaultToggleActionKey);
                }
            }
        }
    }
}
*/

/*

private void LockInteractable(bool _status)
{
if ((int)ObjectType == (int)TypeObject.DoorTogglesGroup)
{
    _targetCloseToggle.interactable = _status;
}
if ((int)ObjectType == (int)TypeObject.SensorTogglesGroup)
{
    _targetOpenToggle.interactable = _status;
}
}


void OnClick(bool _value)
{
if (_value)
{
    if ((int)ObjectType == (int)TypeObject.Toggle && (int)buttonType != (int)ButtonsType.ShopButton && (int)buttonType != (int)ButtonsType.DoorsButton)
    {
        _targetToggle.interactable = false;
    }
    if ((int)buttonType == (int)ButtonsType.ShopButton || (int)buttonType == (int)ButtonsType.DoorsButton)
    {
        _targetToggle.isOn = false;
    }
    if ((int)ObjectType == (int)TypeObject.DoorTogglesGroup || (int)ObjectType == (int)TypeObject.SensorTogglesGroup)
    {
        if (_targetOpenToggle.isOn)
        {
            _targetOpenToggle.interactable = false;
        }

        if (_targetCloseToggle.isOn)
        {
            _targetCloseToggle.interactable = false;
        }
    }

    if (EventManager.isContainsEvents(backActionKey))
    {
        EventManager.TriggerActionEvent(backActionKey);
    }
    else if (EventManager.isContainsEvents(defaultBackActionKey))
    {
        EventManager.TriggerActionEvent(defaultBackActionKey);
    }
    if ((int)ObjectType == (int)TypeObject.Toggle || (int)ObjectType == (int)TypeObject.Button)
    {
        if (EventManager.isContainsEvents("Open" + actionKey))
        {
            EventManager.TriggerActionEvent("Open" + actionKey);
        }
    }
    if ((int)ObjectType == (int)TypeObject.DoorTogglesGroup || (int)ObjectType == (int)TypeObject.SensorTogglesGroup)
    {
        if (_targetOpenToggle.isOn)
        {
            if (EventManager.isContainsEvents("Open" + actionKey))
            {
                EventManager.TriggerActionEvent("Open" + actionKey);
            }
        }

        if (_targetCloseToggle.isOn)
        {
            if (EventManager.isContainsEvents("Close" + actionKey))
            {
                EventManager.TriggerActionEvent("Close" + actionKey);
            }
        }
    }

    EventManager.AddActionListening(backActionKey, () => OnBackAction());
}
else
{
    if ((int)ObjectType == (int)TypeObject.Toggle && (int)buttonType != (int)ButtonsType.ShopButton && (int)buttonType != (int)ButtonsType.DoorsButton)
    {
        _targetToggle.interactable = true;
    }

    if ((int)ObjectType == (int)TypeObject.DoorTogglesGroup || (int)ObjectType == (int)TypeObject.SensorTogglesGroup)
    {
        if (!_targetOpenToggle.isOn)
        {
            _targetOpenToggle.interactable = true;
        }

        if (!_targetCloseToggle.isOn)
        {
            _targetCloseToggle.interactable = true;
        }
    }

    OnBackAction();
}
}

void OnBackAction()
{
if ((int)buttonType != (int)ButtonsType.MenuButton || (int)buttonType != (int)ButtonsType.RewardButton)
{
    if (EventManager.isContainsEvents(backActionKey))
    {
        if (backActionKey == BackActionKeys.InterfaceBackAction.ToString())
        {
            if (EventManager.isContainsEvents(BackActionKeys.RoomsBackAction.ToString()))
            {
                EventManager.TriggerActionEvent(BackActionKeys.RoomsBackAction.ToString());
            }
        }

        EventManager.RemoveActionListening(backActionKey);
        if ((int)ObjectType == (int)TypeObject.Toggle || (int)ObjectType == (int)TypeObject.Button)
        {
            if (EventManager.isContainsEvents("Close" + actionKey))
            {
                EventManager.TriggerActionEvent("Close" + actionKey);
            }
        }
    }
}
}
}
#if UNITYEDITOR
[CustomEditor(typeof(SmartButton))]
class SmartButtonEditor : Editor
{
SerializedProperty _typeObject;
SerializedProperty _buttonType;
SerializedProperty _targetToggle;
SerializedProperty _targetButton;
SerializedProperty _targetOpenToggle;
SerializedProperty _targetCloseToggle;
AnimBool m_ShowToggle;
AnimBool m_ShowButton;
AnimBool m_ShowDoorButtonsGroup;
AnimBool m_ShowSensorButtonsGroup;

protected virtual void OnEnable()
{
_typeObject = serializedObject.FindProperty("_typeObject");
_buttonType = serializedObject.FindProperty("_buttonType");
_targetToggle = serializedObject.FindProperty("_targetToggle");
_targetButton = serializedObject.FindProperty("_targetButton");
_targetOpenToggle = serializedObject.FindProperty("_targetOpenToggle");
_targetCloseToggle = serializedObject.FindProperty("_targetCloseToggle");


m_ShowToggle = new AnimBool(Repaint);
m_ShowButton = new AnimBool(Repaint);
m_ShowDoorButtonsGroup = new AnimBool(Repaint);
m_ShowSensorButtonsGroup = new AnimBool(Repaint);
SetAnimBools(true);
}

void SetAnimBools(bool instant)
{
SetAnimBool(m_ShowToggle, _typeObject.enumValueIndex == (int)TypeObject.Toggle, instant);
SetAnimBool(m_ShowButton, _typeObject.enumValueIndex == (int)TypeObject.Button, instant);
SetAnimBool(m_ShowDoorButtonsGroup, _typeObject.enumValueIndex == (int)TypeObject.DoorTogglesGroup, instant);
SetAnimBool(m_ShowSensorButtonsGroup, _typeObject.enumValueIndex == (int)TypeObject.SensorTogglesGroup, instant);
}

void SetAnimBool(AnimBool a, bool value, bool instant)
{
if (instant)
    a.value = value;
else
    a.target = value;
}

public override void OnInspectorGUI()
{
SetAnimBools(false);

serializedObject.Update();

EditorGUILayout.PropertyField(_typeObject);
EditorGUILayout.PropertyField(_buttonType);

if (EditorGUILayout.BeginFadeGroup(m_ShowToggle.faded))
{
    EditorGUI.indentLevel++;
    EditorGUILayout.PropertyField(_targetToggle);
    EditorGUI.indentLevel--;
}
EditorGUILayout.EndFadeGroup();

if (EditorGUILayout.BeginFadeGroup(m_ShowButton.faded))
{
    EditorGUI.indentLevel++;
    EditorGUILayout.PropertyField(_targetToggle);
    EditorGUI.indentLevel--;
}
EditorGUILayout.EndFadeGroup();

EditorGUILayout.Space();

if (EditorGUILayout.BeginFadeGroup(m_ShowDoorButtonsGroup.faded))
{
    EditorGUI.indentLevel++;
    EditorGUILayout.PropertyField(_targetOpenToggle);
    EditorGUILayout.PropertyField(_targetCloseToggle);
    EditorGUI.indentLevel--;
}
EditorGUILayout.EndFadeGroup();

EditorGUILayout.Space();

if (EditorGUILayout.BeginFadeGroup(m_ShowSensorButtonsGroup.faded))
{
    EditorGUI.indentLevel++;
    EditorGUILayout.PropertyField(_targetOpenToggle);
    EditorGUILayout.PropertyField(_targetCloseToggle);
    EditorGUI.indentLevel--;
}
EditorGUILayout.EndFadeGroup();

EditorGUILayout.Space();

serializedObject.ApplyModifiedProperties();
}
}
#endif

*/
