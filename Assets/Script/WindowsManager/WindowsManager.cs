﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CommonEnums;
using UnityEngine.UI;
using System;

public class WindowsManager : MonoBehaviour
{


    public Windows shop;
    public Windows mainmenu;
    public Windows popupShow;


    public static Action onNavigationButtonClick = delegate { }; 

    public void Start()
    {
        Windows.HideAll();
        Windows.ShowWindows(NavigationButtonType.Main);
        InitAction(); 

    }

    void OnGUI()
    {
        if (Input.GetKeyUp(KeyCode.Home)|| Input.GetKeyDown(KeyCode.Escape))
        {
           CloseCurrentWindows();
        }
    }
    

    public  void InitAction()
    {
        
        EventManager.AddListener(NavigationButtonType.CloseWindows, CloseCurrentWindows);
        foreach (string winName in Enum.GetNames(typeof(NavigationButtonType)))
        {
            if (winName != NavigationButtonType.CloseWindows.ToString())
            EventManager.AddListener(winName, ()=> { Windows.ShowWindows(winName); onNavigationButtonClick.Invoke();  }); 
        }


    }
  
    //  не используйте без крайней необходимости  открытый метод 
    public void CloseCurrentWindows()
    {   
           Windows.CloseCurentWindows();
    }
    public static void CloseCurrentWindowsStatic()
    {
        Windows.CloseCurentWindows();
    }


    public void OpenShop()
    {
        Windows.ShowWindows(WindowsType.Shop , onShowAction: ()=> { Debug.Log("OpenShop");  }); 
    }
    public void OpenMap()
    {
        Windows.ShowWindows(WindowsType.Map, onShowAction: () => { Debug.Log("OpenMap"); }); 
    }
    public void OpenSetting()
    {
        Windows.ShowWindows(WindowsType.Setting); 
    }

    // open for button
    public void OpenWindows(WindowsType type)
    {
        Windows.ShowWindows(type.ToString()); 
    }
    public void OpenWindows(string windows)
    {
        Windows.ShowWindows(windows);
    }

    public void Pause(bool open)
    {
        if (open)  
            Windows.ShowWindows("pause", onShowAction: () => { Debug.Log("Pause"); });
            Time.timeScale = (open)?0f:1f;     
    }
	
	
	// Update is called once per frame
	void Update () {
		
	}

   
}
