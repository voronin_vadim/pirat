﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using CommonEnums; 

using TType = System.UInt16; 

public  class bigint //: //, IComparable, IConvertible, IComparable<Int32>, IEquatable<Int32>
{

   public const int  maxValue = 1000; 
   


    static  int masPow = Enum.GetNames(typeof(BigItemEnum)).Length;

    private List<int> data;


    #region Constructors
    /// <summary>
    /// Create bigInt from  List data
    /// </summary>
    /// <param name="_data"> List of Data</param>
    public bigint(List<int> _data)
    {
        data = _data.Take(masPow).ToList();
        if (data.Count < _data.Count)
        {
            Debug.LogWarning("Enter to much big value on < color = red > BIGINT </ color > param ");
           data =  MaxData(); 
        }
    }

    /// <summary>
    /// Create bigInt from  int
    /// </summary>
    /// <param name="value"> Value in int</param>
    public bigint(int value)
    {
        int level = 0;
        data = Enumerable.Repeat(0, masPow).ToList();
        while (value > 0)
        {
            int ost = value % maxValue;
            data[level] = ost;
            value /= maxValue;
            level++;
            if (level > masPow)
            {
                data[masPow] = maxValue - 1;
                return;
            }
        }

    }
    public bigint(string valueString)
    {

        data = ZeroData();
        int size = 3;
        int step =Mathf.Min(valueString.Length, masPow * size);
        step = Mathf.CeilToInt(step / 3f);
        for (int i = 0; i < step; i++)
        {
            string value = valueString.Substring(i * size,  Mathf.Min( valueString.Length - i*size,  size));

            data[i] = Convert.ToInt32(value); 
        }
    }

    public bigint(int value  , BigItemEnum en)
    {
        data = Enumerable.Repeat(0, masPow - 1).ToList();
        data.Insert((int)en, value);
        Normalize(); 
        
    }

    #endregion

    bigint MaxValue()
    {
        var d= MaxData(); 
        return  new bigint(d); 
    }
    List<int> MaxData()
    {
        return Enumerable.Repeat(maxValue - 1, masPow).ToList(); 
    }
    List<int> ZeroData()
    {
        return Enumerable.Repeat(0, masPow).ToList();
    }
    public string ToSaveString()
    {
        Normalize();
        string rez =   data.Aggregate("", (current, next) => current + next.ToString("000"));

        return rez; 
    }
    public int GetValueIndex(int index)
    {
        Normalize();
        return data[index]; 

    }
    public int MaxPow()
    {
        int pow = 0;
        Normalize();
        for (int i = data.Count - 1; i >= 0; i--)
        {
            if (data[i] > 0)
            {
                pow = i; break;
            }
        }
        return pow; 
    }
    override public string ToString()  //форматер не стал подрубать ,выводит 3 самых значимых числа 
    {
        Normalize(); 
        for (int i = data.Count-1; i >=0; i--)
        {
            if (data[i] > 0)
            {
                if (i == 0)
                    return data[0].ToString();
                else
                {
                    int os = data[i].ToString().Length;
                    string value = data[i].ToString();
                    value += ((os - 3) == 0) ? "" : "," + data[i - 1].ToString("000").Substring(0,3-os);
                    value += ((BigItemEnum)(i)).ToString();
                    return value;
                }
            } 
        }
        return "0"; 
    }
    public static implicit operator bigint(int value)
    {
        return (new bigint(value)); 
    }

    public static bigint  operator +(bigint a, bigint b)
    {
        bigint v = new bigint(a.data.JoinByPosition(b.data, (p, t) => p + t).ToList());
        v.Normalize(); 
        return v;  
       
    }
   

    public static bigint operator -(bigint a, bigint b)
    {
        bigint v = new bigint(a.data.JoinByPosition(b.data, (p, t) => p - t).ToList());
        v.Normalize(); 
        return v;  

    }

    public static bigint operator *(bigint a, int b)
    {
        bigint v = new bigint(a.data);
        v.Mul(b);
        return v;
    }
    public static bigint operator *(int b, bigint a)
    {
        return a*b;
    }


    public void  Mul(float value)
    {
        data = data.Select((p) => (int)(p*value)).ToList();
       /* for (int i=0; i < masPow-1; i++)
        {
            data[i] =(int)(data[i+1] *maxValue* value)  +data[i]; //  понижаем степень, так поточнее будет 
        }*/
        Normalize(); 
    }

    public static bigint Sum(bigint a, bigint b)
    {
        return a + b; 
    }
    public static bigint Sub(bigint a, bigint b)
    {
        return a - b;
    }


    public bool MoreOrEqual(bigint b)
    {
        for (int i = masPow - 1; i >= 0; i--)
        {
            if (data[i] != b.data[i]) return (data[i] > b.data[i]);
           
        }
        return true;
    }

  

    /// <summary>
    ///   Make each value  rand less thah 999
    /// </summary>
    /// <param name="target"></param>
  public   void  Normalize()
    {

        //  delete all value bigger than 999999...
        for (int i = 0; i < masPow; i++)
        {
            while (data[i] >= maxValue)
            {
                data[i] -= maxValue; 
                if (i == masPow-1) {data = MaxData(); return; }
                data[i + 1]++; 
            }
        }
        //delete all value lesss than 0
        for (int i = 0; i < masPow; i++)
        {
            while (data[i] < 0)
            {
                data[i] += maxValue;
                if (i == masPow) { data = ZeroData(); return; }
                data[i + 1]--;
            }
        }
        return; 
    }




}
