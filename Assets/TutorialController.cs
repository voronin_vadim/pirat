﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;
using System.Linq;
using CommonEnums;
using UnityEngine.UI;

public class TutorialController : MonoSingleton<TutorialController>
{

    public Button targetButton;


    public List<TutorStep> tutorStep;
    public int currentStep;
   
    bool showingNow = false;
    bool waitMenuForShow;

    const string tutorSt = "TutorStep";
    static string TUTOR_BROKE = "Tutor break"; 
    static string SAVE_FOR_SHOW = "Tutor save";

    public GameObject coverAll;

    public enum TutorStepType

    {
        PiratClick = 0,
        AchivClick,
        TimerClick,
        LevelClick,
        StarPomClick,
        BuyUpgrade,
        Mission,
        BuyTeam,
        SelecetType,
        BuyIsland

    }
    [System.Serializable]
    public class TutorStep
    {
        public TutorStepType type;
        public List<RectTransform> fingers;
        bool loopClick;
    }


    public void Start()
    {

        Initialize();
    }
    public void Initialize()
    {
        tutorStep.SelectMany(p => p.fingers).ToList().ForEach(x => x.gameObject.SetActive(false));
        currentStep = PlayerPrefs.GetInt(tutorSt, 0);
        if (currentStep == 0)
        {
            StartCoroutine(ShowStepCoroutine(2f, currentStep));
        }
        Windows.OnChangeActiveWindows += ChangeWindows;

        if (PlayerPrefs.GetInt(SAVE_FOR_SHOW, -1) != -1)
        {
            StartCoroutine(ShowStepCoroutine(2f, currentStep));
            
        }
    }
    private void OnDestroy()
    {
        Windows.OnChangeActiveWindows -= ChangeWindows;       
        PlayerPrefs.SetInt(SAVE_FOR_SHOW, (waitMenuForShow || showingNow)? currentStep:-1); 
        
    }
    void ChangeWindows(CommonEnums.NavigationButtonType type)
    {
        if (type == NavigationButtonType.Main)
            MenuShow();
    }
    IEnumerator ShowStepCoroutine(float time, int step)
    {
       // coverAll?.SetActive(true);
       // Instantiate(targetButton, coverAll.transform);
        yield return new WaitForSeconds(time);
        TryShow(tutorStep[step].type);

    }

    public void Paused(TutorStepType type)
    {
        
        TutorStep st = tutorStep.FirstOrDefault(p => p.type == type);
        int ind = 0;
        if (st == null) return;
        ind = tutorStep.IndexOf(st);
        if (ind != currentStep)
            return;

        Debug.Log($"<color =yellow>  TUTOR paused current step + {type} </color>");
        waitMenuForShow = false;
    }

    public void BrokeTutorStep( TutorStepType type)
    {
       if (PlayerPrefs.GetInt(TUTOR_BROKE + type.ToString(), 0) == 0)
        {
            PlayerPrefs.SetInt(TUTOR_BROKE + type.ToString(), 1);
            Debug.Log($"<color =yellow>  TUTOR broke step + {type} </color>");
        }
    }
    /// <summary>
    /// Broke tutor  Use only for broke in init
    /// </summary>
    /// <param name="type"></param>
    public  void StartBrokeTutorStep(TutorStepType type)
    {
        BrokeTutorStep(type);
        
    }

    public void TryShow(TutorStepType type)
    {

        Debug.Log($"TryShow {type}");
        if (showingNow) return;

        TutorStep st = tutorStep.FirstOrDefault(p => p.type == type);
        int ind = 0;
        if (st == null) return;
        ind = tutorStep.IndexOf(st);
        if (ind != currentStep)
            return;

        // ready for show
        if (Windows.curr.Name == CommonEnums.NavigationButtonType.Main)
            ShowTutorStep();
        else
        {
            waitMenuForShow = true;
            Debug.Log($"WaitMain windows for  {type}");
        };

    }

    public void MenuShow()
    {
        if (waitMenuForShow) ShowTutorStep();
    }
    void ShowTutorStep()
    {

        TutorStep st = tutorStep[currentStep];
        showingNow = true;
        waitMenuForShow = false;

        if (PlayerPrefs.GetInt(TUTOR_BROKE + st.type.ToString(), 0) == 1)
        {
            Debug.Log($"<color =yellow>  TUTOR complete step by BROKE + {st.type} </color>");
            CompleteStep(st.type);
            return; 
        }
        foreach (var go in st.fingers)
        {
            go.gameObject.SetActive(true);
          if (go.tag == "move" ) MoveLoop(go);
          else   ClickLoop(go);
           
        }

    }

    

    void HideTutorStep()
    {
        TutorStep st = tutorStep[currentStep];
        showingNow = false;
        foreach (var go in st.fingers)
        {
            go.gameObject.SetActive(false);
            go.DOKill();
        }
    }

    public void CompleteStep(TutorStepType type)
    {
        TutorStep st = tutorStep.FirstOrDefault(p => p.type == type);
        int ind = 0;
        if (st == null) return;
        ind = tutorStep.IndexOf(st);
        if (ind != currentStep)
            return;

        HideTutorStep();
        currentStep++;
        PlayerPrefs.SetInt(tutorSt, currentStep); 

        // imidietly show next step
        if (type == TutorStepType.PiratClick)
        {
            TryShow(TutorStepType.AchivClick);
        }


        // imidietly show next step
        if (type == TutorStepType.AchivClick)
        {
            TryShow(TutorStepType.TimerClick);
        }


        if (type == TutorStepType.TimerClick && LevelController.Instance.CurrentLevelInternal > 0)
        {
            StartCoroutine(ShowStepCoroutine(1f, currentStep)); //TryShow(TutorStepType.LevelClick);
        }
        if (type == TutorStepType.Mission)
        {
            TryShow(TutorStepType.BuyTeam);
        }


    }

    public void ReadyForShow()
    {
    }

    Tween ClickLoop(RectTransform tr)
    {
        return tr.DOScale(1.2f, 0.5f).SetLoops(-1, LoopType.Yoyo);

    }
    Tween MoveLoop(RectTransform tr)
    {
        return tr.DOAnchorPos(new Vector2(0f, -500f), 1f).SetRelative().SetLoops(-1, LoopType.Yoyo);
    }
}
